extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var god = get_node("/root/god")
var cam = null
var window_scale = Vector2()
var original_position
func _ready():

	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	if not cam:
		self.cam = self.god.game.character.cam
	self.position = (self.cam.get_camera_position()) * -0.07
	self.scale =  Vector2(1.5-(self.cam.get_zoom().x)/25,1.5-(self.cam.get_zoom().y)/25)
	self.offset = Vector2(600, 400)
	self.window_scale.x = OS.window_size.x/1920.0 * 500
	self.window_scale.y = OS.window_size.y/1080.0 * 250
	self.offset -= Vector2(self.window_scale.x*(self.scale.x-1.2336), self.window_scale.y*(self.scale.x-1.2336))

	
	pass
