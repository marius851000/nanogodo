extends Node

#10/03/18 Grilvhor enters the team, first push is that comment! :D

var DEFAULT_CONFIG = {
	"Video" : 
	{
		"resolution_width" : 800, 
		"resolution_height" : 480,
		"fullscreen" : false,
		"borderless" : false,
		"resizable" : true,
		"vsync" : true,
	},
	"Audio" :
	{
		"music" : 80,
		"music_mute" : false,
		"sfx" : 80,
		"sfx_mute" : false,
		"ui" : 80,
		"ui_mute" : false
	}
}
var config_dict = {
	"Video" : {},
	"Audio" : {}
}
var cant_use_values
onready var config_file = ConfigFile.new()

# The shitty part :(
var pause_able
var id = 0

#my folders, yo
const CONFIG_FOLDER = "config"
const SAVE_FOLDER = "save"
const LOG_FOLDER = "log"

# My babies
const Game = preload("res://script/Game.gd")
const Menu = preload("res://scene/Menu.tscn")

# For some dev

export(bool) var skip_main_menu
export(bool) var use_pycharm_for_dump
export(String, DIR) var pycharm_path
# Later on we can use this class to load different gods
onready var god = get_node("/root/god")

# Currently started game, or null
var game
var menu
# Image data of the last screen capture
var capture

func _ready():
	recreate_file_tree()
	if skip_main_menu:
		start_new_game(god)
	else:
		start_main_menu()

func start_new_game(god, save_name=null):
	if menu:
		menu._set_capture_texture(menu.nanogodo_background.get_data(),true)
		var t = Timer.new()
		t.set_wait_time(3)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		menu.last_loaded_scene_node.visible = false
		yield(t, "timeout")
	if not save_name:
		game = Game.new(god, self, "001_tutorial", "001_oxygenate")
		add_child(game)
	else:
		load_game(save_name)

func start_main_menu():
	unload_game()
	menu = Menu.instance()
	add_child(menu)

func unload_main_menu():
	if .has_node("Menu"):
		menu.queue_free()

func unload_game():
#	game.propagate_call("queue_free")
#	god.game = null
#	god.board = null
	god.reset_layer()
	if game:
		if game.menu_node:
			game.menu_node.queue_free()
		if game.character:
			game.character.set_process(false)
			game.character.set_process_input(false)
			game.character.set_physics_process(false)
			game.character.queue_free()
		if game.world_node:
			for child in game.world_node.get_children():
				child.queue_free()
			game.world_node.queue_free()
		if game.board:
			game.board.free()
		if .has_node("Game"):
			game.queue_free()


func _capture_game_to_img():
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# Let two frames pass to make sure the screen was captured
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	# Retrieve the captured image
	capture = get_viewport().get_texture().get_data()
	# Flip it on the y-axis (because it's flipped)
	capture.flip_y()


################################################################################
# FILES ########################################################################
################################################################################


# Mainly uselful for display of file paths
func is_windows():
	return OS.get_name() in ["Windows", "WinRT"]

func get_OS_join():
	if not OS.get_executable_path().begins_with("/"):
		return "\\"
	else:
		return "/"

func get_parent_dir(path):
	var i = path.find_last(get_OS_join())
	if i == -1:
		return path
	else:
		return path.substr(0, i+1)

func get_executable_dir():
	return get_parent_dir(OS.get_executable_path())

func get_config_path():
	var config_path
	if OS.is_debug_build():
		if is_running_in_editor():
			config_path = "res://%s/settings.ini" % CONFIG_FOLDER
			return config_path
	config_path = "%s%s%ssettings.ini" % [
		get_executable_dir(), CONFIG_FOLDER,
		get_OS_join()
		]
	return config_path


func get_config_folder_path():
	var config_folder_path
	if OS.is_debug_build():
		if is_running_in_editor():
			config_folder_path = "res://%s/" % CONFIG_FOLDER
			return config_folder_path
	config_folder_path = "%s%s%s" % [
		get_executable_dir(),
		CONFIG_FOLDER, get_OS_join()
	]
	return config_folder_path

func get_save_path(save_name):
	var save_path
	if OS.is_debug_build():
		if is_running_in_editor():
			save_path = ("res://%s/%s%s" % [SAVE_FOLDER, save_name, ".sav" ] )
			return save_path
	save_path = "%s%s%s%s%s" % [
		get_executable_dir(),
		SAVE_FOLDER, get_OS_join(), save_name, ".sav"
	]
	return save_path

func get_save_folder_path():
	var save_folder_path
	if OS.is_debug_build():
		if is_running_in_editor():
			save_folder_path = ("res://%s/" % SAVE_FOLDER)
			return save_folder_path
	save_folder_path = "%s%s%s" % [
		get_executable_dir(),
		SAVE_FOLDER, get_OS_join()
	]
	return save_folder_path

func get_log_folder_path():
	var log_folder_path
	if OS.is_debug_build():
		if is_running_in_editor():
			log_folder_path = ("res://%s/" % LOG_FOLDER)
			return log_folder_path

	log_folder_path = "%s%s%s" % [
		get_executable_dir(),
		LOG_FOLDER, get_OS_join()
	]
	return log_folder_path

func recreate_file_tree():
	var dir = Directory.new()
	if dir.open(get_executable_dir()) == OK:
		print(get_executable_dir())
		var log_folder_path = get_log_folder_path()
		if not dir.dir_exists(log_folder_path):
			if dir.make_dir(log_folder_path) != OK:
				OS.alert("Log folder (%s) is missing and can't be created (right issue?)." % log_folder_path, "Log  missing :(.")
		var config_folder_path = get_config_folder_path()
		if not dir.dir_exists(config_folder_path):
			set_default_settings()
			if dir.make_dir(config_folder_path) != OK:
				OS.alert("Config folder (%s) is missing and can't be created (right issue?)." % config_folder_path, "Config folder missing :(.")
		var save_folder_path = get_save_folder_path()
		if not dir.dir_exists(save_folder_path):
			if dir.make_dir(save_folder_path) !=OK:
				OS.alert("Save folder (%s) is missing and can't be created (right issue?)." % save_folder_path, "Save folder missing :(.")
	else:
		OS.alert("The game is not able to open it's parent directory: %s" % get_executable_dir(), "Cannot open game directory")
################################################################################
# SAVE #########################################################################
################################################################################


var __char_pickle
var __board_pickle

func save_game(save_name):
	pause_able = false
	var __game_pickle = game.to_pickle()
	var save_path = get_save_path(save_name)
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.WRITE, "$=P!nKh47F0r3VeR=$")
	if err == OK:
		var save_string = var2str(__game_pickle)
		f.store_string(save_string)
		f.close()
	else:
		OS.alert(
			"error: The game is not able to write %s, check"  % [save_path] +
			"permissions and if this directory exists\n" +
			"The save file has not been written", 
			"ERROR: Can't write save file"
		)
	pause_able = true

func load_game(save_name):
	pause_able = false
	var save_path = get_save_path(save_name)
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, "$=P!nKh47F0r3VeR=$")
	var game_pickle
	if err == OK:
		var save_string = f.get_as_text()
#		print(savestring)
		game_pickle = str2var(save_string)
		f.close()
		# H0|Y $H!7
		unload_game()
		
#		id += 1
		
#		yield(get_tree(), "idle_frame")
#		yield(get_tree(), "idle_frame")
#		yield(get_tree(), "idle_frame")
#		yield(get_tree(), "idle_frame")
#		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
#		dump_objects(id)
		self.game = Game.from_pickle(god, self, game_pickle)
		unload_main_menu()
		add_child(game)
		
	else:
		OS.alert(
			"error: The game is not able to read '%s', " % save_path +
			"check permissions and if this directory/file exists\n" +
			"The save file has not been read",
			"ERROR: Can't read save file"
		)
#		get_parent().unload_game()
	pause_able = true


################################################################################
# DEBUG #######################################################################
################################################################################


# MEMORY DUMP ###############################################################

func _input(event):
	if Input.is_action_just_pressed("debug_dump_memory_to_log"):
		dump_objects(id, get_log_folder_path(), "memory_dump_")
		id +=1
		print("Dumping")
	if Input.is_action_just_pressed("debug_compare_last_logs"):
		if use_pycharm_for_dump:
			compare(id-2, id-1, get_log_folder_path(), "memory_dump_")

func dump_objects(log_id, log_path, log_slug):
	var f = File.new()
	var id_string = String(log_id)
	var inst_string_buffer
	f.open(log_path + log_slug + id_string + ".log", File.WRITE)
	for i in range(100000):
		var object = instance_from_id ( i )
		if object:
#			var inst_dict = inst2dict(object)
			inst_string_buffer = "\nOBJECT %d: %s" % [i, object.get_class()]
			f.store_string(inst_string_buffer + "\n")
			
			# node debugger
			if object.has_method("get_name"):
				if object.get_name() != "":
					inst_string_buffer = "NAME: %s" % object.get_name()
					f.store_string(inst_string_buffer + "\n")
			
			if object.has_method("get_parent"):
				if object.get_parent():
					inst_string_buffer = "PARENT: %d: %s" %  [object.get_parent().get_instance_id(), object.get_parent().get_name()]
					f.store_string(inst_string_buffer + "\n")
			if object.has_method("is_inside_tree"):
				inst_string_buffer = "IS INSIDE TREE: %s" %  [object.is_inside_tree()]
				f.store_string(inst_string_buffer + "\n")
			
			#signals debugger
			if(object.get_incoming_connections().size() > 0):
				inst_string_buffer = "CONNECTION LIST :"
				f.store_string(inst_string_buffer + "\n")
				for j in range(object.get_incoming_connections().size()):
					inst_string_buffer = "%s" % String(object.get_incoming_connections()[j])
					f.store_string(inst_string_buffer + "\n")
	f.close()

func compare(previous_id, id, log_path, log_slug):
	if previous_id >=0:
		var pycharm_executable
		if is_windows():
			pycharm_executable = "pycharm.bat"
		else:
			pycharm_executable = "pycharm.sh"
	
		var pycharm_executable_path = pycharm_path + pycharm_executable
		var previous_log_dump_path = log_path + log_slug + String(previous_id) + ".log"
		var log_dump_path = log_path + log_slug + String(id) + ".log"
		if File.new().file_exists(pycharm_executable_path):
			OS.execute(pycharm_executable_path, [ "diff", previous_log_dump_path, log_dump_path], false)
		else:
			OS.alert("Pycharm binairies are missing for comparing files, set it up in inspector on %s node" % self.get_name(), "Pycharm is missing :(")
	else:
		OS.alert("Dump at least two times the memory to compare it", "Not enough sample :(")

func is_running_in_editor():
	var dir = Directory.new()
	if File.new().file_exists( "res://project.godot" ) :
		return true
	else:
		return false
#String( object.get_instance_id() ) +

#
#func destroy_world():
#	if has_node("World"):
#		$World.queue_free()
#	if world_node:
#		world_node.queue_free()
#
#func destroy_game_board():
#	if board:
#		board.queue_free()
#		god.board = null






################################################################################
# CONFIG #######################################################################
################################################################################


# CHECK SETTINGS ###############################################################


# GENERAL
func check_loaded_config(section, key):
	if config_file.has_section_key( section, key):
		config_dict[section][key] = config_file.get_value(section, key) 
	else:
		config_dict[section][key] = DEFAULT_CONFIG[section][key]
		config_file.set_value(section, key, config_dict[section][key])


func check_all_loaded_config():
	check_loaded_resolution(config_file)
	for sec in DEFAULT_CONFIG:
		for setting in DEFAULT_CONFIG[sec]:
			if(setting != "resolution_width" && setting != "resolution_height"):
				check_loaded_config(sec, setting)


func check_and_set_config_integrity():
	for sec in config_file.get_sections():
#		print(sec)
		if not DEFAULT_CONFIG.has(sec):
			config_file.erase_section(sec)
		for setting in config_file.get_section_keys(sec):
			if !DEFAULT_CONFIG[sec].has(setting):
				config_file.set_value(sec, setting, null)
			else:
				config_file.set_value(sec, setting, config_dict[sec][setting])


#SCREEN
func check_screen_settings(res):
	var screen_size = OS.get_screen_size(OS.current_screen)
	var screen_rect = Rect2(Vector2(0,0),screen_size)
	if screen_rect.has_point(res):
		return true
	return false

func check_loaded_resolution(config):
	var sec = "Video"
	var keyw = "resolution_height"
	var keyh = "resolution_width"
	if config.has_section_key( sec, keyw) && config.has_section_key( sec, keyh):
		config_dict[sec][keyw] = config.get_value(sec, keyw)
		config_dict[sec][keyh] = config.get_value(sec, keyh)
	else:
		
		config_dict[sec][keyw] = DEFAULT_CONFIG[sec][keyw] 
		config_dict[sec][keyh]= DEFAULT_CONFIG[sec][keyh]
		config.set_value(sec, keyw, config_dict[sec][keyw])
		config.set_value(sec, keyh, config_dict[sec][keyh])


# SET SETTINGS #################################################################


# GENERAL
func set_default_settings():
	config_file = ConfigFile.new()
	for section in DEFAULT_CONFIG:
		for setting in DEFAULT_CONFIG[section]:
#			print(setting)
			config_dict[section][setting] = DEFAULT_CONFIG[section][setting]
			config_file.set_value(section, setting, config_dict[section][setting])
#			print(config.has_section_key( section, setting))


# SCREEN
func set_screen_settings(res, fullscreen, borderless, resizable, vsync):
	borderless = false
	OS.window_resizable = resizable and not fullscreen
	OS.window_fullscreen = fullscreen and not borderless
	OS.window_borderless = borderless and fullscreen
	if not fullscreen:
		OS.window_size = res
		if(OS.window_position.x + res.x >= OS.get_screen_size().x):
			OS.window_position.x = OS.get_screen_size().x - res.x
		
		if(OS.window_position.y + res.y >= OS.get_screen_size().y):
			OS.window_position.y = OS.get_screen_size().y - res.y
	else:
		OS.window_position = OS.get_screen_position()
		OS.window_size = OS.get_screen_size()
#		if not borderless:
#			Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
#		else:
#			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
#		if not fullscreen:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	OS.vsync_enabled = vsync


func set_right_resolution(res):
	var check_res = check_screen_settings(res)
	if not check_res:
		var s_res = OS.get_screen_size(OS.current_screen)
		res = Vector2(min(res.x,s_res.x), min(res.y, s_res.y))
	return res


# CONFIG FILE LOADER ###########################################################


func load_and_check_config_file():
	cant_use_values = false
	var OS_join = get_OS_join()
	get_config_path()
	var err = config_file.load(get_config_path()) # if not, something went wrong with the file loading
	if err == OK: # if not, something went wrong with the file loading
		check_all_loaded_config()
	elif err != ERR_PARSE_ERROR:
		set_default_settings()
		pass
	else:
		set_default_settings()
		print("Parse error, check your config file")
		cant_use_values = true


func save_config_file():
	check_and_set_config_integrity()
	if config_file.save(get_config_path()) != OK:
		print("Can't write config file")


################################################################################
# SNIPPET FOR PROPAGATE CALL ###################################################
################################################################################

#func recursively_own_scene(node):
#	for child in node.get_children():
#		child.propagate_call("set_owner",[node] )
#	node.set_owner(self)

#func save_game_through_nodepacking():
##	unpause_game()
#	var www = self.world_node
##	var www = $World
#	print("a")
#	recursively_own_scene(www)
#	var save_path
#	var packed_scene = PackedScene.new()
#	packed_scene.pack(www)
#	if OS.is_debug_build():
#		save_path = ("res://save/Save.tscn")
#	else:
#		save_path = ("%s/save/Save.tscn" % OS.get_executable_path() )
#	ResourceSaver.save(save_path, packed_scene)
#
#	var d = inst2dict(self)
#	print(d.keys())
#	print(d.values())
#
#
##	pause_game()
#
#func load_game_through_nodepacking():
#	var save_path
#	# Load the PackedScene resource
#	if OS.is_debug_build():
#		save_path = ("res://save/Save.tscn")
#	else:
#		save_path = ("%s/save/Save.tscn" % OS.get_executable_path() )
#	pass
#	var packed_scene = load(save_path)
#	pass
#	# Instance the scene
##	var my_scene = packed_scene.instance(PackedScene.GEN_EDIT_STATE_DISABLED)
#	var my_scene = packed_scene.instance(PackedScene.GEN_EDIT_STATE_MAIN)
#	unpause_game()
#	if has_node("World"):
#		remove_child($World)
#	add_child(my_scene)
