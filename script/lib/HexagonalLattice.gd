# thanks redblobgames <3

# Hex coordinates are named Q and R (analogous to X and Y).
# Note that in the default (mad) Y-reversed screen-painting space,
# with the XY coordinates issued by the rest of Godot,
# Q goes in the direction of 3 o'clock
# R goes in the direction of 5 o'clock
# So Hex(1, -1) is midnight.

# Not sure if there's any way in gdscript to extend Vector2 and Vector3 ?
# Maybe with C classes ?
#class Hex(Vector2):
#	func _init():
#		pass
#class Cube:
#	extends Vector3
#const Hex = preload("res://Hex.gd")


# I'd like to remove this
extends Node

const SQRT_3 = sqrt(3)

static func hex_to_pix(hex, size=1):
	return Vector2(
		size * SQRT_3 * (hex.x + hex.y * 0.5),
		size * 1.5 * hex.y
	)

static func pix_to_hex(xy, size=1):
	var q = (xy.x * SQRT_3/3.0 - xy.y/3.0) / size
	var r = xy.y * 2.0/3.0 / size
	return hex_round(Vector2(q, r))

static func cube_to_axial(cube):
	return Vector2(cube.x, cube.z)

static func axial_to_cube(hex):
	var x = hex.x
	var z = hex.y
	var y = -x-z
	return Vector3(x, y, z)

static func cube_round(cube):
	var rx = round(cube.x)
	var ry = round(cube.y)
	var rz = round(cube.z)

	var x_diff = abs(rx - cube.x)
	var y_diff = abs(ry - cube.y)
	var z_diff = abs(rz - cube.z)

	if x_diff > y_diff and x_diff > z_diff:
		rx = -ry-rz
	elif y_diff > z_diff:
		ry = -rx-rz
	else:
		rz = -rx-ry

	return Vector3(rx, ry, rz)

static func hex_round(hex):
	return cube_to_axial(cube_round(axial_to_cube(hex)))
