
static func merge(basis, patch, strict=true):
	"""
	Merge the two input dictionaries, recursively, into a new dictionary.
	The patch value has priority. Input dictionaries are left unchanged.
	Will throw errors if the merge is ambiguous, when strict.
	Return the merged dictionary.
	/!. Godot limits recursion ; deeply nested dicts may not work as expected.
	"""
	assert typeof(basis) == TYPE_DICTIONARY
	assert typeof(patch) == TYPE_DICTIONARY
	var merged = {}
	for key in basis:
		if patch.has(key):
			var patch_value = patch[key]
			var basis_value = basis[key]
			var patch_is_dict = typeof(patch_value) == TYPE_DICTIONARY
			var basis_is_dict = typeof(basis_value) == TYPE_DICTIONARY
			assert not strict or not (patch_is_dict and not basis_is_dict)
			assert not strict or not (basis_is_dict and not patch_is_dict)
			if patch_is_dict and basis_is_dict:
				merged[key] = merge(basis_value, patch_value)
			else:
				merged[key] = patch_value
		else:
			merged[key] = basis[key]
	for key in patch:
		if not basis.has(key):
			merged[key] = patch[key]
	return merged


static func str_to_vec2(s):
	"""
	Inverse of `JSON.print(Vector2())`.
	Todo: compile the regex only once, in a class const.

		JSON.print(Vector2()) == "(0, 0)"
		str_to_vec2(JSON.print(my_vec2)) == my_vec2
	"""
	var regex = RegEx.new()
	regex.compile("\\(\\s*(-?\\d+),\\s*(-?\\d+)\\s*\\)")
	var result = regex.search(s)
	if not result:
		print("str_to_vec2(): Unrecognized Vector2 string : %s" % s)
		assert 666*999+666+999 != 666999  # read it upside down !
	var q = result.get_string(1).to_int()
	var r = result.get_string(2).to_int()
	
	return Vector2(q, r)


static func reposition_sprite_with_flip_between(
		sprite, scale_y,
		local_a, local_b,
		global_a, global_b
	): # :]
	"""
	
	"""
	assert local_a.y == 0 # not supported atm, adds complexity
	assert local_b.y == 0
	
	# maybe rewrite this cleanly using matrices (transforms)
	
	var is_mirrored = global_a.x > global_b.x
	
	var gab = (global_b - global_a)
	var lab = (local_b - local_a) # hello, gab from the lab !
	
	# POSITION
	assert sprite.is_centered()
	var global_m = 0.5 * (global_a + global_b)
	sprite.set_global_position(global_m)
	
	# ROTATION
	# rotated(self.get_global_rotation())
	var angle_straight_to_global = Vector2(1,0).angle_to(gab)
	var rotation = angle_straight_to_global
	if is_mirrored:
		rotation += 0.5 * TAU
	sprite.set_global_rotation(rotation)
	
	# SCALE
	var scale_x = gab.length() / lab.length()
	sprite.set_global_scale(Vector2(scale_x, scale_y))
	
	# STUP&FLIP H
	sprite.set_flip_h(is_mirrored)

