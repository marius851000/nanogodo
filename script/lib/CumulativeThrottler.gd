extends Node  # we need _process to empty our internal queue

# Unsure if Throttler is the exact term.
# If you know how to do this without extending Node, we're all ears !
# We could also make and use a Timer node, to avoid polling on process.
# We'd still need to extend Node, or require a node as injected dependency.

var callee   # Object
var callback # String
var cooldown # ms
var last_callback_time  # timestamp in ms
var index = Dictionary()
var queue = Array()

func _init(cooldown, callee, callback):
	set_callback(callee, callback)
	set_cooldown(cooldown)

func _ready():
	set_process(true)

func _process(delta):
	try_callback()

func set_callback(callee, callback):
	self.callee = callee
	self.callback = callback
	self.last_callback_time = OS.get_ticks_msec()

func set_cooldown(cooldown):
	self.cooldown = cooldown * 1000.0

func throttle(key, number):
	if index.has(key):
		index[key] += number
	else:
		index[key] = number
		queue.append(key)
		set_process(true)
	try_callback()

func try_callback():
	if queue.size() == 0:
		set_process(false)
		return
	var now = OS.get_ticks_msec()
	if last_callback_time + cooldown < now:
		var key = queue.pop_front()
		var val = index[key]
		index.erase(key)
		callee.call(callback, key, val)
		last_callback_time = now
