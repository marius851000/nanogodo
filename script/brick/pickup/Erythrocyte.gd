extends "res://script/core/Pickup.gd"

# Approximately 2.4 million new erythrocytes (red cells) are produced per second
# These cells have an average volume of about 90 fL (femtoLiters)
# with a surface of about 136 μm2,
# and can swell up to a sphere shape containing 150 fL.

func init(god, config, stack, entropy):
	.init(god, config, stack, entropy)

var _my_rand = rand_range(0, 624)

func _process(delta):
	var t = OS.get_ticks_msec()
	var c = _my_rand / 100.0
	var s = 0.5
	var a = 0.002
	var b = 0.15 * s
	# frisson
#	scale = Vector2(s + sin(t*a+c)*b + abs(cos(t*a*16+c)*b*0.6), s + cos(t*a+c)*b*0.62*0.62)
	scale = Vector2(s + sin(t*a+c)*b + abs(cos(t*a*3+c)*b*0.6), s + cos(t*a+c)*b*0.62*0.62)
