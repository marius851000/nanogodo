extends "res://script/core/Fixture.gd"

# Passive storage unit.
# Has a square grid of slots, each holding a pickup (stack).

################################################################################ 

const Utils = preload("res://script/lib/Utils.gd")
const Inventory = preload("res://script/core/Inventory.gd")
const ChestInteractiveUi = preload("res://script/gui/ChestInteractiveUi.gd")
#const UiFactory = preload("res://script/core/UiFactory.gd")

################################################################################

var slots_x = 10
var slots_y = 10

var inventory  # consider it private?

func _init(
		god, tile=Vector2(0,0), direction=Vector2(1, 0),
		slots_x=10, slots_y=10
	): # :)
	"""
	Watch out for unpickle() as well if you edit the _init() API signature.
	"""
	self.fixture_type = "chest"
	self.god = god
	self.hex_position = tile
	self.hex_direction = direction # irrelevant
	self.slots_x = int(slots_x) # not sure that implicit sanitization is best
	self.slots_y = int(slots_y)
	self.inventory = Inventory.new(slots_x, slots_y, true, null)
	connect_to_inventory() # ideally done in set_inventory

func _ready():
	set_texture(preload("res://data/base/graphics/fixture/chest/chest-spritesheet.png"))
	set_centered(true)
	set_hframes(6)
#	set_offset(Vector2(7,-7))
#	reorient_sprite()
	var _scale = 0.42
	set_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.25 / _scale)
	
	#prints("CHsEST", self.name)  # OH GOD WHY -- BRB


### PICKLING ###################################################################

# Override parent method
#func set_hex_direction(hex):
#	self.hex_direction = hex
#	reorient_sprite()

static func from_pickle(god, rick):
	var chest = new(
		god,
		rick.hex_position,
		rick.hex_direction,
		rick.slots_x,
		rick.slots_y
	)
	inject_pickle(chest, rick)
	if rick.has('inventory'):
		chest.disconnect_from_inventory()
		chest.inventory = Inventory.from_pickle(
			god, rick.inventory
		)
		chest.connect_to_inventory()
	return chest

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {
		'slots_x':   slots_x,
		'slots_y':   slots_y,
	})
	if not for_blueprint:
		pickle['inventory'] = inventory.to_pickle()
	
	return pickle


### LOOP #######################################################################

#func on_machinery_tick(progress):
#	if progress == 1.0 and OS.is_debug_build(): #benchmark which should go first
#		check_internal_statuses()


### FIXTURE INTERFACE ##########################################################

func can_feed_pickup(pickup, xy):
	return inventory.has_room_for_pickup(pickup)

func feed_pickup(pickup, xy):
	inventory.store(pickup)
	if pickup.get_parent():
		pickup.get_parent().remove_child(pickup)
#	if interactive_ui:
#		interactive_ui.update_display()

func can_provide_pickup(pickup_type=null, amount=1):
	return inventory.has_enough(pickup_type, amount)

func provide_pickup(pickup_type=null, amount=1):
	return inventory.retrieve(pickup_type, amount)

func get_all_pickups():
	return self.inventory.get_all_pickups()


### INVENTORY ##################################################################

func connect_to_inventory():
	self.inventory.connect("pickups_added",   self, "on_pickups_added")
	self.inventory.connect("pickups_removed", self, "on_pickups_removed")
	self.inventory.connect("pickups_moved",   self, "on_pickups_moved")

func disconnect_from_inventory():
	self.inventory.disconnect("pickups_added",   self, "on_pickups_added")
	self.inventory.disconnect("pickups_removed", self, "on_pickups_removed")
	self.inventory.disconnect("pickups_moved",   self, "on_pickups_moved")

func on_pickups_added(pickup_type, amount):
	update_display()

func on_pickups_removed(pickup_type, amount):
	update_display()

func on_pickups_moved():
	update_display()


### GUI ########################################################################

var interactive_ui
func open_interactive_ui():
	var title = name
	interactive_ui = ChestInteractiveUi.new(god, self, title, inventory)
	interactive_ui.name = "%sInteractiveUi" % title
	
	god.get_interactive_hud_layer().add_child(interactive_ui)
	update_chest_frame()

func close_interactive_ui():
	if interactive_ui:
		interactive_ui.close()
		interactive_ui = null
		update_chest_frame()

func get_interactive_ui():
	return interactive_ui

func is_interactive_ui_open():
	return null != interactive_ui

func update_interactive_ui():
	assert interactive_ui
	interactive_ui.update_display()


### PAINTING ###################################################################

func update_display():
	update_chest_frame()
	if is_interactive_ui_open():
		update_interactive_ui()
	# If the hand carries one of the slots of this inventory,
	# it needs to be updated as well.
	# also, what about a god.hand ?
	god.game.character.hand.update_display()

func update_chest_frame():
	self.frame = 0 # closed and not full at all sprite 0
	if get_interactive_ui():
		self.frame += 1 # sprite 1 open and not full
	if self.inventory.has_slots_full(): 
		if self.inventory.is_full(): 
			self.frame += 4 # sprite 4 and 5 totally full 
		else:
			self.frame += 2 # close and not totally full sprite 2 and 3

#func reorient_sprite():
#	pass
#	var directions = god.board.HEX_DIRECTIONS
#	var i = directions.find(to_intake)
#	assert i > -1  # to_outake in directions (cheaper, though harder to read)
#	frame = SPRITE_META[i][0]
#	rotation_degrees = SPRITE_META[i][1]


### DEBUG ######################################################################

func get_dump():
	var desc = "%s\n" % self.name
	desc += "  at %s\n" % hex_position
	desc += "  of size %d×%d\n" % [slots_x, slots_y]
	if inventory:
		desc += "  has %d pickups\n" % inventory.count()
		desc += "%s\n" % inventory.to_string()
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc

