extends "res://script/core/Fixture.gd"

#const ConveyorBelt = preload("res://script/node/ConveyorBelt.gd")
const Pickup = preload("res://script/core/Pickup.gd")
const Utils = preload("res://script/lib/Utils.gd")

# Pickup currently handled
var pickup

# In pixel land
var hand_sprite
var _hand_position  # Vector2(x,y), private, use setter and getter
var hand_grab_radius = 20
var hand_grab_radius_squared = hand_grab_radius * hand_grab_radius

# Unit vector in hex space
var to_intake = Vector2(-1, 0)  # left
var to_outake = Vector2( 1, 0)  # right

# Array of (frame, angle) ; angle is clockwise from 3 o'clock
# We don't have enough frames for all directions so we cheat. Shadows tell.
const SPRITE_META = [
	Vector2(0, 0), Vector2(0, 60), Vector2(0,60),
	Vector2(0, 0), Vector2(0, -60), Vector2(0,-60)
]

const STATE_UNPOWERED = 0
const STATE_MOVING_TO_INTAKE = 1
const STATE_WAITING_FOR_INTAKE = 2
const STATE_MOVING_TO_OUTAKE = 3
const STATE_WAITING_FOR_OUTAKE = 4

# useful for debug
#const STATE_UNPOWERED = 'STATE_UNPOWERED'
#const STATE_MOVING_TO_INTAKE = 'STATE_MOVING_TO_INTAKE'
#const STATE_WAITING_FOR_INTAKE = 'STATE_WAITING_FOR_INTAKE'
#const STATE_MOVING_TO_OUTAKE = 'STATE_MOVING_TO_OUTAKE'
#const STATE_WAITING_FOR_OUTAKE = 'STATE_WAITING_FOR_OUTAKE'

var state = STATE_UNPOWERED

onready var general_layer = god.get_general_layer()

# Waypoints for pickups, in pixel land
#var wp_start
#var wp_end

# internal pickup physics
#const PICKUP_SIZE = 4.0
#const PICKUP_STEP = 1/16.0

# to_outake : Vector2(1, 0)
func _init(god, tile=Vector2(0,0), to_intake=Vector2(1,0), to_outake=null):
	"""
	Watch out for unpickle() as well if you edit the _init() API signature.
	"""
	self.fixture_type = "inserter"
	self.god = god
	self.hex_position = tile
	self.hex_direction = to_intake
	self.to_intake = to_intake
	if not to_outake:
		to_outake = -1.0 * to_intake
	self.to_outake = to_outake
	
	state = STATE_MOVING_TO_INTAKE

func _ready():
	assert god.board
	
	# SEE SPRITE_META const as well
	set_texture(preload("res://data/base/graphics/fixture/inserter/inserter-body-spritesheet.png"))
	set_offset(Vector2(6,-4))
	set_hframes(2)
	var _scale = 0.500
	set_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.2 / _scale)
	
	hand_sprite = Sprite.new()
	hand_sprite.name = "InserterHand"
	hand_sprite.set_texture(preload("res://data/base/graphics/fixture/inserter/inserter-hand-test.png"))
#	hand_sprite.set_scale(Vector2(1.0, 1.0))
#	hand_sprite.set_offset(Vector2(0, 0))
	add_child(hand_sprite)
	reorient_sprite()
	
	if null == get_hand_position():
		set_hand_position(get_resting_hand_position())


### PICKLING ###################################################################

static func from_pickle(god, rick):
	var inserter = new(
		god,
		rick.hex_position,
		rick.to_intake,
		rick.to_outake
	)
	inject_pickle(inserter, rick)
	if rick.has('hand_position'):
		inserter.set_hand_position(rick.hand_position, false)
	if rick.has('state'):
		inserter.state = rick.state
	if rick.has('pickup'):
		#print("Pickup from pickle ", rick.pickup)
		inserter.pickup = Pickup.from_pickle(god, rick.pickup)
		inserter.add_to_layer(inserter.pickup)
	
	return inserter

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {
		'to_intake': to_intake,
		'to_outake': to_outake,
	})
	
	if not for_blueprint:
		pickle['state'] = state
		if pickup:
			pickle['pickup'] = pickup.to_pickle()
#			#print(name, " pickled pickup : ", pickle['pickup'])
		if Vector2(0,0) == relative_to:
			pickle['hand_position'] = get_hand_position()
	
	return pickle


### ORIENTATION ################################################################

# Override parent method
func set_hex_direction(hex):
	self.hex_direction = hex
	self.to_intake = hex
	self.to_outake = -1.0 * hex
#	recompute_intake()
#	recompute_waypoints()
	reorient_sprite()
	if pickup:
		state = STATE_MOVING_TO_OUTAKE
	else:
		state = STATE_MOVING_TO_INTAKE


### LOOP #######################################################################

var _currently_targeted_pickup_on_ground_wr
func on_machinery_tick(progress):
	if progress == 1.0 and OS.is_debug_build(): #benchmark which should go first
		check_internal_statuses()

	if state == STATE_UNPOWERED:
		return
	elif state == STATE_MOVING_TO_INTAKE:
		assert not pickup
		move_to_intake()
		if is_at_intake():
			state = STATE_WAITING_FOR_INTAKE
	elif state == STATE_WAITING_FOR_INTAKE:
		var ground_pickup
		var intake_pickup
		if _currently_targeted_pickup_on_ground_wr:
			var targeted_pickup = _currently_targeted_pickup_on_ground_wr.get_ref()
			if targeted_pickup:
				if (targeted_pickup.position - \
					get_resting_hand_position()).length_squared() < \
					hand_grab_radius_squared:
					ground_pickup = targeted_pickup
				else:
					_currently_targeted_pickup_on_ground_wr = null
		if not ground_pickup:
			ground_pickup = get_pickup_on_ground_in_range_of_resting_hand()
		if ground_pickup:
			_currently_targeted_pickup_on_ground_wr = weakref(ground_pickup)
			move_to_grab(ground_pickup)
			if can_grab(ground_pickup):
				god.board.remove_from_grid_if_on_it(ground_pickup)
				grab(ground_pickup)
				assert pickup
				_currently_targeted_pickup_on_ground_wr = null
				state = STATE_MOVING_TO_OUTAKE
		else:
			for intake_fixture in get_intake_fixtures():
				var pickup_type = null
				var amount = 1
				if intake_fixture.can_provide_pickup(pickup_type, amount):
					if intake_fixture is god.fixtures.belt.script:
						intake_pickup = intake_fixture.get_one_pickup()
						move_to_grab(intake_pickup)  # for belts
						if can_grab(intake_pickup):
							intake_fixture.free_pickup(intake_pickup)
							grab(intake_pickup)
							assert pickup
							state = STATE_MOVING_TO_OUTAKE
							break
					else:
						intake_pickup = intake_fixture.provide_pickup(pickup_type, amount)
						grab(intake_pickup)
						assert pickup
						state = STATE_MOVING_TO_OUTAKE
						break

		if not (ground_pickup or intake_pickup):
			move_to_intake()

	elif state == STATE_MOVING_TO_OUTAKE:
		assert pickup
#		if not pickup:
#			print("/!. SMELL Pickup lost by %s while moving to outake." % \
#				 self.name)
#			state = STATE_MOVING_TO_INTAKE
		move_to_outake()
		if is_at_outake():
			state = STATE_WAITING_FOR_OUTAKE
	elif state == STATE_WAITING_FOR_OUTAKE:
		assert pickup
		if can_drop():
			drop()
			state = STATE_MOVING_TO_INTAKE
		else:  # for rotations
			move_to_outake()
	else:
		assert false # state not recognized
	
#	reposition_hand()

#func on_pickup_picked(pickup, tile):
##	if tile == hex_position:
#	if self.pickup == pickup:
#		free_pickup()
#	else:
#		print("%s picked from %s but not in %s." % [pickup.name, tile, self])


### STATUSES ###################################################################

const H_P_E = 6  # Hand Position Epsilon, squared

func has_pickup():
	return null != pickup

func is_at_intake():
	#glmatrix needed. (this feels inefficient)
	return (get_intake_position()-get_hand_position()).length_squared() < H_P_E

func is_at_outake():
	#glmatrix needed. (this feels inefficient)
	return (get_outake_position()-get_hand_position()).length_squared() < H_P_E

func get_intake_position():
	return god.board.hex_to_pix(hex_position + to_intake) #memoization

func get_outake_position():
	return god.board.hex_to_pix(hex_position + to_outake) #memoization

func get_intake_fixtures():
	return god.board.get_tile_fixtures(hex_position + to_intake)

func set_hand_position(xy, do_reposition_sprite=true):
	_hand_position = xy
	if do_reposition_sprite:
		reposition_hand_sprite(xy)

func get_hand_position():
	return _hand_position

func get_resting_hand_position():
	return god.board.hex_to_pix(hex_position + to_intake)

#class NodeClosenessSorter:  # not used anymore, too slow, see below
#	var position
#	func _init(position):
#		self.position = position
#	func sort(a, b):
#		return (a.position-position).length_squared() < \
#		       (b.position-position).length_squared()

func get_pickup_on_ground_in_range_of_hand():
	var pickups = god.board.get_pickups_on_grid_in_circle(
		get_hand_position(), hand_grab_radius
	)
	if pickups:
		# we can shave sorting, for perfs ⋅⋅⋅ (wow, huge impact)
#		var sorter = NodeClosenessSorter.new(hand_position)
#		pickups.sort_custom(sorter, "sort")
		return pickups[0]
	return null

func get_pickup_on_ground_in_range_of_resting_hand():
	var pickups = god.board.get_pickups_on_grid_in_circle(
		get_resting_hand_position(), hand_grab_radius
	)
	if pickups:
		# we can shave sorting, for perfs ⋅⋅⋅ (wow, huge impact)
#		var sorter = NodeClosenessSorter.new(hand_position)
#		pickups.sort_custom(sorter, "sort")
		return pickups[0]
	return null

func get_all_pickups():
	if self.pickup:
		return [self.pickup]
	return Array()

### ACTIONS ####################################################################

#func free_pickup():
##	print("Freeing pickup %s from %s" % [pickup.name, self.name])
#	pickup = null

func move_to_intake():
	move_hand_to(get_intake_position())

func move_to_outake():
	move_hand_to(get_outake_position())

func move_to_grab(target_pickup):
	move_hand_to(target_pickup.position)

func move_hand_to(xy):
	var speed = 0.18
	if state == STATE_WAITING_FOR_INTAKE:
		speed = 0.31
	var next_position = get_hand_position().linear_interpolate(xy, speed)
	set_hand_position(next_position)

func can_grab(target_pickup):
	return (target_pickup.position - get_hand_position()).length_squared() < 5

func add_to_layer(pickup):
	god.add_to_general_layer(pickup)

func grab(target_pickup):
	assert target_pickup
	assert not pickup  # already has a pickup ?
	pickup = target_pickup
	pickup.pickable = false
	pickup.position = get_hand_position()
	add_to_layer(pickup)
#	if pickup.get_parent():
#		pickup.get_parent().remove_child(pickup)
#	general_layer.add_child(pickup)
	
#	god.emit_signal("pickup_grabbed", pickup,
#		god.board.pix_to_hex(pickup.position)
#	)

func can_drop():
#	prints(name, "CAN DROP?", god.board.can_drop(pickup, pickup.position))
	return god.board.can_drop(pickup, get_hand_position())

func drop():
	assert pickup
	pickup.pickable = true
#	prints("DrOPpiNg On BoArd", god.board.name, pickup.position)
	god.board.do_drop(pickup, get_hand_position())
	pickup = null


### PAINTING ###################################################################

const LIFE_UNIVERSE_EVERYTHING = 42
const DEATH_VOID_NOTHING = -42

func reposition_hand_sprite(xy):
	var texture_size = hand_sprite.texture.get_size()
#	var hand_local_start = Vector2(0.15, 0.23) * texture_size
#	var hand_local_end = Vector2(0.85, 0.66) * texture_size
	var hand_local_start = Vector2(0.08, 0) * texture_size
	var hand_local_end = Vector2(0.92, 0) * texture_size
	
	var shoulder_global = get_global_transform() \
			.xform(Vector2(0,DEATH_VOID_NOTHING)) # coincidence? I THINK NOT!
#			.linear_interpolate(xy, 0.09)
	
	var hand_global_start = shoulder_global + 8 * (xy - shoulder_global).normalized()
	
	Utils.reposition_sprite_with_flip_between(
		hand_sprite, 1.0,
		hand_local_start, hand_local_end,
		hand_global_start, xy
	)
	var hsgs = hand_sprite.get_scale()
	var hys = max(0.5,min(1.0, 1.0 / (hsgs.x)))
	hand_sprite.set_scale(Vector2(hsgs.x, hys))
	
	#reorient_hand_sprite(xy)
	if pickup:
		pickup.position = xy

#func reorient_hand_sprite(xy):
#	if hand_sprite.position.x < 0:
#		hand_sprite.flip_h = true
#	else:
#		hand_sprite.flip_h = false

func reorient_sprite():
	var directions = god.board.HEX_DIRECTIONS
	var i = directions.find(to_intake)
	self.frame = 1
	assert i > -1  # to_outake in directions (cheaper, though harder to read)
	if i >= 2 and i <= 4:
		self.frame = 1
		i =  (3 + i) % 6 # (5 + 2 - i + 2) % 6 
		rotation_degrees = SPRITE_META[i][0]
	else:
		self.frame = 0
	rotation_degrees = SPRITE_META[i][1]


### DEBUG ######################################################################

func get_dump():
	var desc = "%s\n" % self.name
	if pickup:
		desc += "  carries %s (%s)\n" % [pickup.name, pickup]
	desc += "  from %s to %s\n" % [to_intake+hex_position, to_outake+hex_position]
	desc += "  hand is at %s\n" % get_hand_position()
	desc += "  state is %s\n" % state
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc

func check_internal_statuses():
	if pickup and not pickup.is_inside_tree():
		printerr("Found orphan %s %s in %s"%[pickup.name, pickup, self.name])
		assert false


#const TICKS_DELAY_MS = 250
#const TICKS_BULLY_MS = 19
#var last_tick_at = null
#func _process(delta):
#	var now = OS.get_ticks_msec()
#	if null == last_tick_at:
#		last_tick_at = now
#	if now - last_tick_at > TICKS_DELAY_MS and now % TICKS_DELAY_MS < TICKS_BULLY_MS:
#		last_tick_at = now # - (now%TICKS_DELAY_MS) or something
##		print("tic")
#		for pickup in get_children():
#			pickup.rotate(TAU*0.13) # Ô joy !
#			var a = 500
#			pickup.position = Vector2(a * cos(now),a * sin(now))
#			pickup.translate(hl.hex_to_pix(to_next) * 0.03)

