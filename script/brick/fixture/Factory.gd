extends "res://script/core/Fixture.gd"

const FactoryInteractiveUi = preload("res://script/gui/FactoryInteractiveUi.gd")
const Inventory = preload("res://script/core/Inventory.gd")

const CALLBACK_INVENTORY = "on_inventory_change"

var intake_inventories = Array()
var outake_inventories = Array()
var recipe_type  # a key in the dict of recipes
var recipe  # the actual recipe dict (from god.recipes)
var animation_speed = 1.0 # frames per (working) machinery tick

func _init(god, tile=Vector2(0,0), direction=Vector2(1,0), recipe_type=null):
	"""
	Watch out for unpickle() as well if you edit the _init() API signature.
	"""
	self.fixture_type = "factory"
	self.god = god
	self.hex_position = tile
	self.hex_direction = direction # irrelevant
	set_recipe_type(recipe_type)

const SPRITE_HFRAMES = 12

func _ready():
	set_texture(preload("res://data/base/graphics/fixture/factory/factory_spritesheet_gimp.png"))
#	set_texture(preload("res://data/base/graphics/fixture/factory/factory_spritesheet_gimp.png"))
	set_hframes(SPRITE_HFRAMES)
	set_centered(true)
	
	var _scale = 0.777
	set_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.45 / _scale)

#func _draw():
#	draw_rect(Rect2(-50,-50,100,100), Color(1.0,0,0,0.2))


### OVERRIDES ##################################################################

# Override parent method
#func set_hex_direction(hex):
#	self.hex_direction = hex
#	reorient_sprite()

static func from_pickle(god, rick):
	var factory = new(
		god,
		rick.hex_position,
		rick.hex_direction,
		rick.recipe_type
	)
	
	if rick.has('intake_inventories'):
		factory.intake_inventories = []
		for inventory_pickle in rick.intake_inventories:
			factory.intake_inventories.append(
				Inventory.from_pickle(god, inventory_pickle)
			)
	if rick.has('outake_inventories'):
		factory.outake_inventories = []
		for inventory_pickle in rick.outake_inventories:
			factory.outake_inventories.append(
				Inventory.from_pickle(god, inventory_pickle)
			)
	
	return factory

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {
		'recipe_type': recipe_type,
	})
	
	if not for_blueprint:
		var iip = []
		for inventory in self.intake_inventories:
			iip.append(inventory.to_pickle())
		pickle['intake_inventories'] = iip
		
		var oip = []
		for inventory in self.outake_inventories:
			oip.append(inventory.to_pickle())
		pickle['outake_inventories'] = oip
	
	return pickle


### LOOP #######################################################################

const STATE_UNPOWERED          = 0
const STATE_WAITING_FOR_INTAKE = 1
const STATE_CREATING           = 2
const STATE_WAITING_FOR_OUTAKE = 3

# useful for early debug
#const STATE_UNPOWERED = 'STATE_UNPOWERED'
#const STATE_WAITING_FOR_INTAKE = 'STATE_WAITING_FOR_INTAKE'
#const STATE_CREATING = 'STATE_CREATING'
#const STATE_WAITING_FOR_OUTAKE = 'STATE_WAITING_FOR_OUTAKE'

var state = STATE_UNPOWERED

var _creation_start_time = 0
var craft_progress = 0.0

func on_machinery_tick(progress):
	if not recipe:
		return
	
	if OS.is_debug_build() and progress == 1.0: #benchmark which should go first
		check_internal_statuses()
	
	#animate() # debug toggle
	
	if state == STATE_UNPOWERED:
		# todo: check power status?
		state = STATE_WAITING_FOR_INTAKE
	elif state == STATE_WAITING_FOR_INTAKE:
		if has_enough_intake_pickups_for_creation():
			state = STATE_CREATING
			_creation_start_time = 0
			craft_progress = 0
	elif state == STATE_CREATING:
		if not has_room_in_outakes():
			state = STATE_WAITING_FOR_OUTAKE
		else:
			if 0 == _creation_start_time:
				_creation_start_time = OS.get_ticks_msec()
				reserve_intake_pickups()
			var now = OS.get_ticks_msec()
			
			animate()
			craft_progress = 1.0 * (now - _creation_start_time) / recipe.time
			
			if get_interactive_ui():
				update_interactive_ui()
			if craft_progress >= 1.0:
				create_pickups_into_outakes()
				consume_reserved_intake_pickups()
				_creation_start_time = 0
				state = STATE_WAITING_FOR_INTAKE
	elif state == STATE_WAITING_FOR_OUTAKE:
		if has_room_in_outakes():  # todo: make it lazy
			state = STATE_WAITING_FOR_INTAKE
	else:
		assert false


### STATUSES ###################################################################

func get_craft_progress():
	return craft_progress

func get_all_pickups():
	var pickups = []
	for inventory in intake_inventories:
		for i in range(inventory.get_all_pickups().size()):
			pickups.append(inventory.get_all_pickups()[i])
	
	for inventory in outake_inventories:
		for i in range(inventory.get_all_pickups().size()):
			pickups.append(inventory.get_all_pickups()[i])
	for pickup in _reserved_intake_pickups:
		pickups.append(pickup)
	
	return pickups

func has_room_in_outakes():
	for config in recipe.outakes:
		var has_room = false
		for inventory in outake_inventories:
			if inventory.has_room_for(config.pickup_type, config.quantity):
				has_room = true
				break
		if not has_room:
			return false
	return true

func is_any_outake_full():  # not useful (yet?)
	for inventory in outake_inventories:
		if inventory.is_full():
			return true
	return false

func has_enough_intake_pickups_for_creation():
	for config in recipe.intakes:
		var has_room = false
		for inventory in intake_inventories:
			if inventory.has_enough(config.pickup_type, config.quantity):
				has_room = true
				break
		if not has_room:
			return false
	
	return true


### ACTIONS ####################################################################

#func free_pickup(pickup):
#	print("Freeing pickup %s from %s" % [pickup.name, self.name])
#	var k = inventory.find_key_of(pickup)
#	if k:  # it's a Vector2, but Vector2(0,0) evaluates to false
#		inventory.remove(pickup, k)
#	else:
#		printerr("/!. SMELL : Freeing %s absent from %s…" % [pickup.name, name])

func can_feed_pickup(pickup, xy):
	"""
	Whether this fixture can be fed the pickup at position xy.
	We don't care about xy in factories.
	"""
	for inventory in intake_inventories:
		if inventory.has_room_for_pickup(pickup):
			return true
	
	return false

func feed_pickup(pickup, xy):
	var stored = false
	for inventory in intake_inventories:
		if inventory.has_room_for_pickup(pickup):
			inventory.store(pickup)
			stored = true
			break
	
	assert stored # or death
	if pickup.get_parent():
		pickup.get_parent().remove_child(pickup)
#	if get_interactive_ui():
#		update_interactive_ui()

#func get_providable_pickup(pickup_type=null, amount=1):
#	assert null == pickup_type # not implemented
#	assert amount == 1 # not implemented
#	return inventory.find(pickup_type, amount)

func can_provide_pickup(pickup_type=null, amount=1):
	assert null == pickup_type # not implemented
	assert amount == 1 # not implemented
	for inventory in outake_inventories:
		if inventory.count() > 0:
			return true
	
	return false

func provide_pickup(pickup_type=null, amount=1):
	assert null == pickup_type # not implemented
	assert amount == 1 # not implemented
	var pickup = null
	for inventory in outake_inventories:
		if inventory.has_enough(pickup_type, amount):
			pickup = inventory.retrieve(pickup_type, amount)
			break
	assert pickup
	#return pickup
#	if is_interactive_ui_open():
#		update_interactive_ui()
	
	return pickup


### INTERACTIVE GUI ############################################################

var interactive_ui
func open_interactive_ui():
	assert null == interactive_ui # or memleaks may happen
	var title = name
	interactive_ui = FactoryInteractiveUi.new(god, self, title, god.recipes, intake_inventories, outake_inventories)
	interactive_ui.name = "%sInteractiveUi" % title
	
	god.get_interactive_hud_layer().add_child(interactive_ui)

func close_interactive_ui():
	if is_interactive_ui_open():
		get_interactive_ui().close()
		interactive_ui = null

func get_interactive_ui():
	return interactive_ui

func is_interactive_ui_open():
	return null != get_interactive_ui()

func update_interactive_ui():
	if is_interactive_ui_open():
		get_interactive_ui().update_display()

func on_inventory_change(change, pickup_type, quantity):
	if is_interactive_ui_open():
		update_interactive_ui()
	# If the hand carries one of the slots of this inventory,
	# it needs to be updated as well.
	god.game.character.hand.update_display()


### INNER LOGIC ################################################################

func forget_recipe_type():
	set_recipe_type(null)
	var iui = get_interactive_ui()
	if iui:
		iui.update_iotakes(intake_inventories, outake_inventories)
		iui.update_display()

func set_recipe_type(recipe_type):
	if self.recipe_type == recipe_type:
		return
	self.recipe_type = recipe_type
	empty_inventories()
	if null == recipe_type:
		self.recipe = null
		intake_inventories = Array()
		outake_inventories = Array()
		return
	assert god.recipes.has(recipe_type)
	var recipe = god.recipes[recipe_type]
	self.recipe = recipe
	
	for intake in recipe.intakes:
		var inventory = Inventory.new(1, 1, true, intake.pickup_type, self, CALLBACK_INVENTORY)
		intake_inventories.append(inventory)
	for outake in recipe.outakes:
		var inventory = Inventory.new(1, 1, true, outake.pickup_type, self, CALLBACK_INVENTORY)
		outake_inventories.append(inventory)
	state = STATE_WAITING_FOR_INTAKE
	craft_progress = 0
	_creation_start_time = 0
	var iui = get_interactive_ui()
	if iui:
		iui.update_iotakes(intake_inventories, outake_inventories)
		iui.update_display()

func get_recipe_type():
	return self.recipe_type

func empty_inventories():
	# Don't despawn the pickups, add them to the player's inventory ?
#	for inventory in self.intake_inventories:
#		for slot in inventory.slots:
#			god.despawn_pickup(inventory.slots[slot])
#	for inventory in self.outake_inventories:
#		for slot in inventory.slots:
#			god.despawn_pickup(inventory.slots[slot])
	self._reserved_intake_pickups = []
	self.intake_inventories = []
	self.outake_inventories = []


### RECIPE'S INTAKE PICKUPS ####################################################

var _reserved_intake_pickups = []
func reserve_intake_pickups():
	assert not _reserved_intake_pickups
	for config in recipe.intakes:
		
		var found = false
		for inventory in intake_inventories:
			if inventory.has_enough(config.pickup_type, config.quantity):
				var p = inventory.retrieve(config.pickup_type, config.quantity)
				_reserved_intake_pickups.append(p)
				found = true
				break
		assert found

func consume_reserved_intake_pickups():
	if get_interactive_ui():
		get_interactive_ui().unhook_hand()

	for pickup in _reserved_intake_pickups:
		god.despawn_pickup(pickup)
	_reserved_intake_pickups = []

func create_pickups_into_outakes():
	for config in recipe.outakes:
		var p = god.spawn_pickup(config.pickup_type, config.quantity)
		assert p.pickup_type  # make sure pickup_type is set in _init, not _ready
		for inventory in outake_inventories:
			if inventory.has_room_for(p.pickup_type, p.stack):
				inventory.store(p)
				break


### PAINTING ###################################################################

#func reorient_sprite():
#	pass
#	var directions = god.board.HEX_DIRECTIONS
#	var i = directions.find(to_intake)
#	assert i > -1  # to_outake in directions (cheaper, though harder to read)
#	frame = SPRITE_META[i][0]
#	rotation_degrees = SPRITE_META[i][1]

var animation_cursor = 0.0
func animate():
	animation_cursor += animation_speed
	var aci = 0
	if animation_cursor >= 1.0:
		aci = 1
		animation_cursor = 0.0
	set_frame((get_frame() + aci) % SPRITE_HFRAMES)


### DEBUG ######################################################################

func get_dump():
	var desc = "%s\n" % self.name
	desc += "  at %s\n" % hex_position
	if recipe_type:
		desc += "  recipe %s\n" % recipe_type
	else:
		desc += "  no recipe\n" % recipe_type
	desc += "  state = %s\n" % state
	var i = 0
	if intake_inventories:
		desc += "  INTAKE(S)\n"
		i = 0
		for inventory in intake_inventories:
			i += 1
			desc += "  #%d\n" % i
			desc += "    has %d pickups\n" % inventory.count()
			desc += "------------------\n"
			desc += "%s\n" % inventory.to_string()
			desc += "------------------\n"
	if outake_inventories:
		desc += "  OUTAKE(S)\n"
		i = 0
		for inventory in outake_inventories:
			i += 1
			desc += "  #%d\n" % i
			desc += "    has %d pickups\n" % inventory.count()
			desc += "------------------\n"
			desc += "%s\n" % inventory.to_string()
			desc += "------------------\n"
	
#	if inventory:
#		desc += "  has %d pickups\n" % inventory.count()
#		desc += "  %s\n" % inventory.to_string()
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc

func check_internal_statuses():
	pass
#	if pickup and not pickup.is_inside_tree():
#		print("Found orphan %s %s in %s"%[pickup.name, pickup, self.name])
#		free_pickup()
