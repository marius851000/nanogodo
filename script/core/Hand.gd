extends Sprite

var god
var character

var held_inventory # Inventory or null
var held_inventory_slot # Vector2 in inventory space

var label

func _init(god, character):
	self.god = god
	self.character = character
	self.name = "Hand"
	self.label = Label.new()
	self.label.rect_position = Vector2(10, -4)
	add_child(self.label)

func is_empty():
	return null == held_inventory

func get_held_pickup():
	assert held_inventory
	return held_inventory.find_at(held_inventory_slot)

func retrieve_pickup():
	assert held_inventory
	var held_inventory = self.held_inventory
	var held_slot = self.held_inventory_slot
	drop()
	var pickup = held_inventory.retrieve_from_slot(held_slot, 0)
	update_display() # better too much than not enough
	return pickup

func retrieve_one_pickup():
	assert held_inventory
	var held_inventory = self.held_inventory
	var held_slot = self.held_inventory_slot
	var held_pickup = get_held_pickup()
	var held_pickup_stack = held_pickup.stack
	if held_pickup_stack == 1:
		drop()
	var pickup = held_inventory.retrieve_from_slot(held_slot, 1)
	update_display()
	return pickup

func grab(inventory, slot):
	assert null == held_inventory
	self.held_inventory = inventory
	self.held_inventory_slot = slot
	update_display()


func drop(): # deprecated, confusing
	return clear()

func clear(strict=true):
	if strict:
		assert self.held_inventory
	self.held_inventory = null
	self.held_inventory_slot = null
	update_display()

func clear_if_empty():
	if not get_held_pickup():
		drop()

func is_shown():
	return true == visible

func show():
	visible = true

func hide():
	visible = false

func update_display():  # noob pitfall: calling this `update()`
	if is_empty():
		self.texture = null
		self.label.text = ""
		hide()
	else:
		var pickup = get_held_pickup()
		if pickup:
			self.texture = pickup.texture
			self.label.text = str(pickup.stack)
			show()
		else:
			# We still point to an inventory slot that has been emptied
			# by external forces. Let's empty the hand as well.
			self.texture = null
			self.label.text = ""
			hide()

func update_position(new_position=null):
	if null == new_position:
		new_position = get_global_mouse_position()
	self.position = new_position

func _input(event):
	if not held_inventory or not is_shown():
		return
	if event is InputEventMouseMotion:
		update_position(event.position)
#		print(Input.get_last_mouse_speed())

#func _process(delta):
#	if held_inventory and texture:
#		frame = (frame + 1) % HAND_SPRITE_HFRAMES