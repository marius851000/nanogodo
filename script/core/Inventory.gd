# This is not a Node.
# It holds the logic pertaining to inventories, in the abstract sense.
# It is still very much a work in progress.

const Pickup = preload("res://script/core/Pickup.gd")

# Events, for callbacks. Enum? Signals?
const EVENT_PICKUPS_ADDED   = 0
const EVENT_PICKUPS_REMOVED = 1
const EVENT_PICKUPS_MOVED   = 2

# Vector2(x,y) => Pickup
# /!\ xy indices start at 1
#     since Vector2(0,0) evaluates to false, and I like to use shorthand.
var slots = Dictionary()
var slots_size_x = 1
var slots_size_y = 1
var stackable = true  # Should pickups be stacked in each slot ?
var pickup_type_constraint = null  # When set, only accept pickups of this type.
var callee # try with signals?
var callback


### SIGNALS ####################################################################

signal pickups_added(type, amount)
signal pickups_removed(type, amount)
signal pickups_moved()


### CONSTRUCTOR ################################################################

func _init(
	slots_size_x=1, slots_size_y=1,
	stackable=true, pickup_type=null,
	callee=null, callback=""
):
	"""
	stackable: Boolean
		Whether pickups can stack in the slots of this inventory.
		Not fully supported yet.
	pickup_type: String
		Only pickups of this type shall be allowed in this inventory.
		Not fully supported yet.
	callee: Object
		When set, will be called on any change to the inventory contents.
		Still not sure why we're using this design pattern and not signals. ?
	callback: String
		Name of the function that will be called on the callee on any change
		to the inventory contents. If callee is set, this must also be.
		The function will be passed :
			- the event type (EVENT_PICKUPS_XXXX)
			- the pickup type (as string, as usual)
			- the quantity
	"""
	self.slots_size_x = slots_size_x
	self.slots_size_y = slots_size_y
	self.stackable = stackable
	self.pickup_type_constraint = pickup_type
	self.callee = callee
	self.callback = callback


# PICKLING #####################################################################

static func from_pickle(god, rick, callee=null, callback=""):
	var inventory = new(
		rick.slots_size_x,
		rick.slots_size_y,
		rick.stackable,
		rick.pickup_type,
		callee, callback
	)
	
	for slot in rick.slots:
		var pickulpe = rick.slots[slot] # pickulpe fiction avec des cornichons
		var pickup_type = pickulpe.pickup_type
		var stack = pickulpe.stack
		
		inventory.slots[slot] = Pickup.from_pickle(god, pickulpe)
		inventory.slots[slot].position = pickulpe.position
	
	return inventory

func to_pickle():
	var slots_pickle = {}
	for slot in slots:
		slots_pickle[slot] = slots[slot].to_pickle()
	
	return {
		'slots_size_x': slots_size_x,
		'slots_size_y': slots_size_y,
		'stackable':    stackable,
		'pickup_type':  pickup_type_constraint,
		'slots':        slots_pickle,
	}

func copy():
	return from_pickle(god, self.to_pickle())

func destroy():
	for slot in slots:
		# which is best?
		# god.despawn_pickup(slots[slot])
		slots[slot].queue_free()
	slots = null
	#.free() # nonexistent

func free():
	print("I AM A FREE BANANA.") # …seems like this is not called.


### STATUSES ###################################################################

func count():
	var n = 0
	for slot in slots:
		n += slots[slot].stack
	return n

func size():
	return slots_size_x * slots_size_y

func has_slots_full():
	if slots.size() < size():
		return false
	else:
		return true

func is_full():
	"""
	Returns whether this inventory is full or not.
	"""
	if not stackable:
		return slots.size() == self.size()
	else:
		if slots.size() < self.size():
			return false
		else:
			for slot in slots:  # no garanteed order, but we don't need one
				if not slots[slot].is_fully_stacked():
					return false
		return true

func is_empty():
	return slots.size() == 0

func is_stackable():
	return self.stackable

func has_pickup_type_constraint():
	return null != self.pickup_type_constraint

func get_pickup_type_constraint():
	return self.pickup_type_constraint

func get_x_range(): # memoize me
	return range(1, self.slots_size_x + 1)

func get_y_range(): # memoize me
	return range(1, self.slots_size_y + 1)

func can_store_type(pickup_type):
	if pickup_type_constraint and pickup_type_constraint != pickup_type:
		return false
	return true

func has_room_for_pickup(pickup):
	var pickup_type = pickup.pickup_type
	var amount = pickup.stack
	return has_room_for(pickup_type, amount)

func has_room_for(pickup_type, amount=1):
	if not can_store_type(pickup_type):
		return false
	var amount_left = amount
	var stack_size = god.pickups[pickup_type].stack
	for y in range(1, slots_size_y+1):
		if 0 == amount_left:
			break
		for x in range(1, slots_size_x+1):
			var k = Vector2(x, y)
			if not slots.has(k):
				if stackable:
					amount_left = max(0, amount_left - stack_size)
				else:
					amount_left = max(0, amount_left - 1)
			elif stackable:
				var slot_pickup = slots[k]
				if not slot_pickup.is_fully_stacked() \
					and slot_pickup.is_of_type(pickup_type):
					amount_left = max(0, amount_left - (stack_size - slot_pickup.stack))
			if 0 == amount_left:
				break
	return 0 == amount_left

func has_room_for_pickups(pickups):
	assert self.stackable
	var amounts = Array()
	for pickup in pickups:
		amounts.append(pickup.stack)
	
	# First, let's check the occupied slots, so we can fill them to the brim
	for slot in slots:
		var slot_pickup = slots[slot]
		var slot_room = slot_pickup.config.stack - slot_pickup.stack
		if slot_room == 0:
			continue
		for i in range(pickups.size()):
			if slot_room == 0:
				break
			if amounts[i] == 0:
				continue
			var pickup = pickups[i]
			if not pickup.is_of_same_type_as(slot_pickup):
				continue
			var stored_count = min(slot_room, amounts[i])
			amounts[i] -= stored_count
			slot_room -= stored_count
	
	# Then, we check all the empty slots
	for slot in get_empty_slots():
		for i in range(pickups.size()):
			if amounts[i] == 0:
				continue
			amounts[i] = 0
			break
	
	var amount_left = 0
	for amount in amounts:
		amount_left += amount
	return 0 == amount_left

func get_slots_with_room(pickup_type=null, amount=1):
	var found_slots = Array()
	var amount_left = amount
	
	# not exactly can_store()
	if null != pickup_type and null != self.pickup_type_constraint:
		if pickup_type != self.pickup_type_constraint:
			assert false
			return null
	
	# First, let's check the occupied slots, so we can fill them to the brim
	if self.stackable and null != pickup_type:
		for slot in self.slots:
			var slot_pickup = self.slots[slot]
			var slot_room = slot_pickup.get_room_left_in_stack()
			if slot_room == 0:
				continue # skip full slots
			if null != pickup_type and not slot_pickup.is_of_type(pickup_type):
				continue # skip slots with different pickup type
			if slot_room < amount_left:
				amount_left -= slot_room
			else:
				amount_left = 0
			found_slots.append(slot)
	
	# Then, we check all the empty slots
	#for slot in get_empty_slots(): # inefficient but clearer
	if amount_left > 0:
		for y in get_y_range():
			for x in get_x_range():
				var slot = Vector2(x, y)
				if not slots.has(slot):
					found_slots.append(slot)
					if self.stackable:
						amount_left = 0
					else:
						amount_left -= 1
				if amount_left == 0:
					break
			if amount_left == 0:
				break
	
	if amount_left > 0:
		assert false
		found_slots = null
	
	return found_slots

func get_slot_with_room(pickup_type=null): # deprecated
	if pickup_type_constraint and pickup_type != pickup_type_constraint:
		return null
	for y in range(1, slots_size_y+1):
		for x in range(1, slots_size_x+1):
			var k = Vector2(x, y)
			if not slots.has(k):
				return k
			elif stackable:
				var slot_pickup = slots[k]
				if not slot_pickup.is_fully_stacked() \
					and slot_pickup.is_of_type(pickup_type):
					return k
	return null

func get_empty_slots():
	var empty_slots = []
	for y in range(1, slots_size_y+1):
		for x in range(1, slots_size_x+1):
			var slot = Vector2(x, y)
			if not slots.has(slot):
				empty_slots.append(slot)
	return empty_slots

func all_slots():
	var all_slots = [] # todo: memoize with staleness on slots_size_*
	for y in range(1, slots_size_y+1):
		for x in range(1, slots_size_x+1):
			all_slots.append(Vector2(x, y))
	return all_slots


func get_all_pickups():
	return slots.values()

func find(pickup_type=null, amount=1):
	if null != pickup_type_constraint and null != pickup_type \
		and pickup_type_constraint != pickup_type:
		return null
	assert amount == 1 # to implement
	for y in range(1, slots_size_y+1):
		for x in range(1, slots_size_x+1):
			var k = Vector2(x, y)
			if slots.has(k):
				var pickup = slots[k]
				if pickup_type:
					if pickup.is_of_type(pickup_type):
						return pickup
				else:
					return pickup
	return null

func find_at(xy):
	"""
	xy: Vector2
		Remember: x and y indices start at 1
	"""
	if self.slots.has(xy):
		return self.slots[xy]
	return null

func store(pickup, where=null, should_fire_callbacks=true):
	"""
	where: Vector2 or Array of Vector2
		One xy slot coordinates, or a list of them.
		If nothing is provided, will try to find available slots and will crash
		if not enough can be found.
	"""
	var pickup_type = pickup.pickup_type
	var pickup_stack = pickup.stack # cache it, 'cause we change it
	if self.pickup_type_constraint:
		assert pickup_type == pickup_type_constraint
	
	var care_where = (null != where)
	if null == where:
		where = get_slots_with_room(pickup.pickup_type, pickup.stack)
	elif not (typeof(where) == TYPE_ARRAY):
		where = [where]
	assert where
	
	var should_despawn_pickup = false
	
	var amount_left = pickup.stack
	if not self.stackable:
		assert pickup.stack == 1  # not implemented
		for slot in where:
			assert not self.slots.has(slot)
			slots[slot] = god.spawn_pickup(pickup_type, 1)
			amount_left -= 1
		assert amount_left == 0
		should_despawn_pickup = true
	#		if should_fire_callbacks:
	#			fire_store_callback(pickup.pickup_type, 1)
	else:
		for slot in where:
			assert amount_left >= 0
			if amount_left == 0:
				break
			if not slots.has(slot):
				slots[slot] = pickup
				amount_left = 0
	#			if should_fire_callbacks:
	#				fire_store_callback(pickup.pickup_type, pickup.stack)
			else:
				var stored_pickup = slots[slot]
				assert stored_pickup.is_of_type(pickup_type)
				assert not stored_pickup.is_fully_stacked()
				
				var room_left = stored_pickup.get_room_left_in_stack()
				var stored_amount = min(room_left, pickup.stack)
				
				amount_left -= stored_amount
				stored_pickup.stack += stored_amount
				pickup.stack -= stored_amount
				
				if pickup.stack == 0:
					should_despawn_pickup = true
				
#				var max_stack = god.pickups[pickup.pickup_type].stack
#				if care_where and stored_pickup.stack + pickup.stack > max_stack:
#					assert not "allowed to oveflow stack of specified slot"
#
#				# Loop through available slots until we stored everything
#				var done = false
#				while (not done) and (null != where):
#					if not slots.has(where):
#						slots[where] = pickup
#						done = true
#						break
#
#					stored_pickup = slots[where]
#					var available_room = max_stack - stored_pickup.stack
#					if available_room < pickup.stack:
#						pickup.stack -= available_room
#						stored_pickup.stack += available_room
#					elif available_room == pickup.stack:
#						pickup.stack = 0 # useless?
#						stored_pickup.stack = max_stack
#						done = true
#						should_despawn_pickup = true
#						break
#					else: # available_room > pickup.stack
#						stored_pickup.stack += pickup.stack
#						pickup.stack = 0 # useless? (also, we use it above)
#						done = true
#						should_despawn_pickup = true
#						break
#
##					where = get_slot_with_room(pickup_type)
#
#				if not done:
#					assert not "allowed to store() when there's not enough room"
#					# You probably ended down here by storing to a full inventory.
#					# We need to make can_store() smarter !
	
	if should_despawn_pickup:
		god.despawn_pickup(pickup)
	
	if should_fire_callbacks:
		fire_store_callback(pickup_type, pickup_stack)

func store_all(inventory):
	var stored_pickup_types = Dictionary() # pickup_type => amount
	for intake_slot in inventory.slots.keys():  # .keys() 'cause we erase() !
		var intake_pickup = inventory.slots[intake_slot]
		var intake_pickup_type = intake_pickup.pickup_type
		var amount_to_store = intake_pickup.stack
		var amount_stored = 0
		for slot in all_slots():
			if not slots.has(slot):
				# Empty slot ? Let's shove the whole thing in it.
				slots[slot] = intake_pickup
				inventory.slots.erase(intake_slot)
				amount_stored = intake_pickup.stack
			else:
				pass # to implement
			if amount_stored == amount_to_store:
				break
		
		if amount_stored > 0:
			if stored_pickup_types.has(intake_pickup_type):
				stored_pickup_types[intake_pickup_type] += amount_stored
			else:
				stored_pickup_types[intake_pickup_type] = amount_stored
	
	if stored_pickup_types.size() > 0:
		for spt in stored_pickup_types:
			inventory.fire_retrieve_callback(spt, stored_pickup_types[spt])
			fire_store_callback(spt, stored_pickup_types[spt])

func move_to_inventory(from_slot, to_inventory, to_slot):
	"""
	Silently ignores moving from empty slots.
	Silently ignores moving between incompatible pickup types.
	If the receiving slot does not have enough room for all the pickups,
	the remainder will stay in the providing slot. (todo)
	"""
	var from_pickup = find_at(from_slot)
	
	if not from_pickup:
		return
	
	var to_pickup = to_inventory.find_at(to_slot)
	
	var internal_move = (self == to_inventory)
	var should_fire_events = (not internal_move)
	
	if not to_pickup:
		# We're free to drop the incoming pickup here
		if to_inventory.stackable:
			remove(from_pickup, from_slot)
			if should_fire_events:
				fire_retrieve_callback(
					from_pickup.pickup_type, from_pickup.stack
				)
			to_inventory.store(from_pickup, to_slot, should_fire_events)
			if internal_move:
				fire_change_callback()
		else:
			assert not "implemented" # told you this was a WiP
	else:
		assert not "implemented"

func exchange_with_inventory(that_slot, with, with_slot):
	
	assert self.slots.has(that_slot)
	assert with.slots.has(with_slot)
	var that_pickup = self.find_at(that_slot)
	var with_pickup = with.find_at(with_slot)
	var cant_store = false
	if self.pickup_type_constraint:
		if not with_pickup.pickup_type == self.pickup_type_constraint:
			cant_store = true
	if with.pickup_type_constraint:
		if not that_pickup.pickup_type == with.pickup_type_constraint:
			cant_store = true
	if not cant_store:
		var internal_exchange = (self == with)
		var should_fire = (not internal_exchange)
		that_pickup = self.retrieve_from_slot(that_slot, 0, should_fire)
		with_pickup = with.retrieve_from_slot(with_slot, 0, should_fire)
		
		#self.store_at_slot(with_pickup, that_slot, should_fire)
	
		self.store(with_pickup, that_slot, should_fire)
		with.store(that_pickup, with_slot, should_fire)
		if internal_exchange:
			self.fire_change_callback()

	
#	inventory.remove(slot_pickup, slot_key)
#	var hand_held_inventory = hand.held_inventory
#	var hand_inventory_slot = hand.held_inventory_slot
#	var internal_exchange = (hand_held_inventory == self.inventory)
#	hand_held_inventory.move_to_inventory(
##				move_pickups_between_inventories(
##					hand_held_inventory,
#		hand_inventory_slot,
#		self.inventory, slot_key
#	)
##				hand.drop()
##				hand.grab(hand_held_inventory, hand_inventory_slot)
#	hand_held_inventory.store(
#		slot_pickup, hand_inventory_slot, not internal_exchange
#	)

func has_enough(pickup_type=null, amount=1):
	if pickup_type_constraint and pickup_type:
		if pickup_type != pickup_type_constraint:
			return false
	assert amount > 0  # don't even try, baby
	var amount_pending_guarantee = amount
	var amount_guaranteed = 0
	for y in range(1, slots_size_y+1):
		if amount_pending_guarantee == 0:
			break
		for x in range(1, slots_size_x+1):
			var slot = Vector2(x, y)
			if not slots.has(slot):
				continue
			var slot_pickup = slots[slot]
			if pickup_type and not slot_pickup.is_of_type(pickup_type):
				continue
			pickup_type = slot_pickup.pickup_type
			
#			var stack_size = god.pickups[pickup_type].stack
#			amount_pending_guarantee = min(amount_pending_guarantee, stack_size)
			
			if slot_pickup.stack <= amount_pending_guarantee:
				amount_pending_guarantee -= slot_pickup.stack
				amount_guaranteed += slot_pickup.stack
#				_delete_pickup_at_slot(slot)
			else:
#				slot_pickup.stack -= amount_pending_guarantee
				amount_guaranteed += amount_pending_guarantee
				amount_pending_guarantee = 0
			
			if amount_pending_guarantee == 0:
				break

	return amount_guaranteed == amount

func retrieve(pickup_type=null, amount=1, should_fire_callbacks=true):
	"""
	Returns a Pickup with a stack size up to `amount`.
	Filters by `pickup_type` if provided, or return first found in reading order.
	Removes the retrieved amount of pickups from the inventory.
	"""
	if pickup_type_constraint:
		if pickup_type:
			assert pickup_type == pickup_type_constraint
		else:
			pickup_type = pickup_type_constraint  # we may comment that out
	assert amount > 0  # don't even try, baby
	var amount_pending_retrieval = amount
	var amount_retrieved = 0
	for y in get_y_range():
		if amount_pending_retrieval == 0:
			break
		for x in get_x_range():
			var slot = Vector2(x, y)
			if not self.slots.has(slot):
				continue
			var slot_pickup = self.slots[slot]
			if pickup_type and not slot_pickup.is_of_type(pickup_type):
				continue
			
			pickup_type = slot_pickup.pickup_type
			
			# why do we do this, again?
			var stack_size = god.pickups[pickup_type].stack
			amount_pending_retrieval = min(amount_pending_retrieval, stack_size)
			###########################
			
#			assert slot_pickup.stack > 0
			
			if slot_pickup.stack <= amount_pending_retrieval:
				amount_pending_retrieval -= slot_pickup.stack
				amount_retrieved += slot_pickup.stack
				_delete_pickup_at_slot(slot)
			else:
				slot_pickup.stack -= amount_pending_retrieval
				amount_retrieved += amount_pending_retrieval
				amount_pending_retrieval = 0
			
			if amount_pending_retrieval == 0:
				break

	if amount_retrieved > 0:
		#assert pickup_type  # weak
		# I still don't get how god found its way here… The miracle of faith!
		if should_fire_callbacks:
			fire_retrieve_callback(pickup_type, amount_retrieved)
		return god.spawn_pickup(pickup_type, amount_retrieved)
	
	assert false  # no silent errors
	return null  # …not sure which is best, really

func retrieve_from_slot(slot, amount=0, should_fire_callbacks=true):
	"""
	amount: Integer
		Size of the stack we want from the slot.
		When set to zero, it means we want all there is. (the default)
	"""
	assert amount >= 0
	assert slots.has(slot)  # or return null?

	var slot_pickup = slots[slot]
	var pickup_type = slot_pickup.pickup_type
	var stack_size = god.pickups[pickup_type].stack
	
	assert slot_pickup.stack >= amount # maybe later support half-ass retrieves
	
	if amount == 0:  # we want everything
		amount = slot_pickup.stack
	
	var remainder = slot_pickup.stack - amount
	if 0 == remainder:
		_delete_pickup_at_slot(slot)
		if should_fire_callbacks:
			fire_retrieve_callback(pickup_type, amount)
	elif 0 < remainder:
		slot_pickup.stack -= amount
		if should_fire_callbacks:
			fire_retrieve_callback(pickup_type, amount)
	else:
		assert not "<փ⋅" # amount retrieved is too high
	
	return god.spawn_pickup(pickup_type, amount)

func _delete_pickup_at_slot(slot):
	var deleted_pickup = slots[slot]
	slots.erase(slot)
	god.despawn_pickup(deleted_pickup)

func remove(pickup=null, k=null):
	assert k or pickup
	if not k:
		k = find_key_of(pickup)
	assert k
	slots.erase(k)

func find_key_of(pickup):
	for y in range(1, slots_size_y+1):
		for x in range(1, slots_size_x+1):
			var k = Vector2(x, y)
			if slots.has(k) and slots[k] == pickup:
				return k
	return Vector2(0, 0)


### CALLBACKS ##################################################################

func fire_store_callback(pickup_type, quantity):
	fire_callback(EVENT_PICKUPS_ADDED, pickup_type, quantity)
	emit_signal("pickups_added", pickup_type, quantity)

func fire_retrieve_callback(pickup_type, quantity):
	fire_callback(EVENT_PICKUPS_REMOVED, pickup_type, quantity)
	emit_signal("pickups_removed", pickup_type, quantity)

func fire_change_callback():
	fire_callback(EVENT_PICKUPS_MOVED, null, 0)
	emit_signal("pickups_moved")

func fire_callback(event, pickup_type, quantity):
	if callee:
		assert callback
		callee.call(callback, event, pickup_type, quantity)


### DEBUG ######################################################################

func to_string():
	var s = ""
	if pickup_type_constraint:
		s += "Only stores %s\n" % pickup_type_constraint
	for y in range(1, slots_size_y+1):
		for x in range(1, slots_size_x+1):
			var slot = Vector2(x, y)
			if slots.has(slot):
				var p = slots[slot]
				s += "%s : %d %s\n" % [slot, p.stack, p.display_name]
	return s.substr(0, s.length()-1)  # remove trailing \n
	
