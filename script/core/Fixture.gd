# The "abstract" base class for all fixtures
# A fixture is occupying one (or more) tiles on the hex lattice.

# A fixture's _init() definition must start with god, tile, direction
# You can add more variables afterwards, but always start with these three.
# Even if `direction` is meaningless. We mean it !
# We're open to suggestions as to how to improve this.
# Notably, we should probably use an overridable init() (without underscore)
# and not allow fixtures to override _init()

################################################################################

extends Sprite

const Utils = preload("res://script/lib/Utils.gd")
const GameBoard = preload("res://script/core/GameBoard.gd")

# Bad design. God is basically the game configuration context and entity spawner.
# We're contemplating passing it to the constructor, which is also bad design,
# but less so, as we'll be able to define different gods in different files.
# Or perhaps we could make /root/god a god-loader instead ?
onready var god = get_node("/root/god")


### PROPERTIES #################################################################

# Slug of the fixture in the global configuration
# Any child class MUST set this, and it MUSt exist or hell will break loose.
var fixture_type

# Whether this fixture can be destroyed by enemies or such
var destructible = true
var health_points = 100
var max_health_points = 100

# Whether this fixture can be removed by a character|player
var pickable = true

# Notes about hexes
# In hex space (axial C.S.) these are Vector2(Q, R)
# Q goes to 3 o'clock, R to 5 o'clock.

# Central|Main|Origin position of this fixture on the global hex lattice
var hex_position = Vector2(0, 0)

# Direction of this fixture. MUST be a unit vector.
# By default, face the east, as sprites do.
var hex_direction = Vector2(1, 0)

# Extra positions this fixture occupies, **in local space** where this fixture's
# position is (0, 0) and its direction is (1, 0).
# Use this to configure your fixtures, but don't read from it, prefer using
# get_hex_positions() to get all the positions occupied by this fixture in
# global space on the board's hex lattice.
# By default, this is empty, so a fixture occupies only one tile.
var hex_extra_positions = []


### POSITIONING INTERFACE ######################################################

func get_hex_positions(origin=null):
	"""
	All the hex positions his fixture occupies, in global space.
	By default this returns an array with a single Vector2 element.
	"""
	if null == origin:
		origin = self.hex_position
	var hex_positions = [origin]
	
	if self.hex_extra_positions:
		# Rotate in local space -around (0,0)- using the fixture direction.
		var extra_positions = GameBoard.rotate_positions_by_direction(
			self.hex_extra_positions, self.hex_direction
		)
		# Offset the results to get the extra positions in global space.
		for i in range(extra_positions.size()):
			extra_positions[i] = extra_positions[i] + origin
	
	return hex_positions


### INTAKE INTERFACE ###########################################################

func can_feed_pickup(pickup, xy):
	"""
	Whether this fixture can be fed the pickup at position xy.
	"""
	return false

func feed_pickup(pickup, xy):
	assert not "Override feed_pickup() in %s." % self.name


### OUTAKE INTERFACE ###########################################################

func can_provide_pickup(pickup_type=null, amount=1):
	return false

func provide_pickup(pickup_type=null, amount=1):
	"""
	Must:
	- return a Pickup instance (that may never have been added to the scene tree)
	- remove the pickup from this fixture's internal memory
	"""
	assert not "Override provide_pickup() in %s." % self.name


# deprecated. Used by belts only. Use provide methods above instead.
func free_pickup(pickup):
	assert not "Override free_pickup() in %s." % self.name


### PICKUPS INTERFACE ##########################################################

func get_all_pickups():
	return Array()


### COLLISION ##################################################################

func add_circular_collidable_body(radius):
	"""
	Radius is expressed as a coefficient of the haxagonal lattice size.
	"""
	assert god.board
	var static_body = StaticBody2D.new()
	var collision_shape = CollisionShape2D.new()
	var circle_shape = CircleShape2D.new()
#	while(not god.board):
#		yield(get_tree(),"idle_frame")
	circle_shape.radius = god.board.hex_lattice_size * radius
	collision_shape.shape = circle_shape
	static_body.add_child(collision_shape)
	add_child(static_body)


### UI DISPLAY #################################################################

func open_interactive_ui():
	pass

func close_interactive_ui():
	pass

func get_interactive_ui():
	return null


### SERIALIZATION INTERFACE ####################################################

# TBD
# inject_base_pickle *
# inject_pickle_base
# inject_pickle_commons
# hydrate
# hydrate_commons
# hydrate_into
static func inject_pickle(target, pickle):
	if pickle.has('name'):
		target.name = pickle.name
	if pickle.has('destructible'):
		target.destructible = pickle.destructible
	if pickle.has('pickable'):
		target.pickable = pickle.pickable

static func from_pickle(god, rick):
	"""
	Should create a new instance of the fixture and return it. (it's a factory)
	God is the current game context. Not the best design, but convenient.
	Rick is a pickle, of course.
	"""
	assert not "Override static from_pickle() in the Fixture fed %s." % rick

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	"""
	Should return serialization-ready, unpicklable data.
	- no Nodes, Resources, etc.
	- nothing but primitives (dicts and arrays of primitives are ok)
	- Vectors are okay too, since we're serializing with `var2str()` (not JSON)
	Extend this, and use `Utils.merge()` and `.to_pickle()`. Eg: Chest.gd
	"""
	assert self.fixture_type
	return {
		'fixture_type':  self.fixture_type,
		'hex_position':  self.hex_position - relative_to,
		'hex_direction': self.hex_direction,
		'destructible':  self.destructible,
		'pickable':      self.pickable,
		'name':          self.name,
	}


### COPY #######################################################################

func copy():
	return from_pickle(god, to_pickle())


### BORING SETGET STUFF ########################################################

# We'll use this for staleness markers, to optimize ?

func set_hex_direction(qr):
	hex_direction = qr

func get_hex_direction():
	return hex_direction
