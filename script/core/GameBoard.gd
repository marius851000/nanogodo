extends Node

# The game board knows the state of the world :
# - where the pickups are on the ground on the cells of the square grid
#   each of these pickups knows its own xy global position for display
# - where the fixtures are on the tiles of the hex lattice
# - who the players are (and where they are) <- not sure

const HexagonalLattice = preload("res://script/lib/HexagonalLattice.gd")

const SQRT_3 = sqrt(3)
const PICKUP_RADIUS = 6
const PICKUP_GRID_RES = Vector2(4,4)

const HEX_DIRECTIONS = [  # clockwise from 3 o'clock
	Vector2( 1, 0), Vector2( 0, 1), Vector2(-1, 1),
	Vector2(-1, 0), Vector2( 0,-1), Vector2( 1,-1)
]

# move to Game singleton
#var players = {
#	'local': {
#		'0': null
#	}
#}

# Sparse dictionaries of keys
# QR Tile Vector2 => array of Fixtures
var fixtures_lattice = {}
# QR Tile Vector2 => array of Grounds
var grounds_lattice = {}
# XY Cell Vector2 => Pickup
var pickups_on_grid = {}

var god
var hex_lattice_size = 1
func _init(god, hex_lattice_size=1):
	self.god = god
	self.hex_lattice_size = int(hex_lattice_size)
	self.name = "GameBoard#%d" % OS.get_ticks_msec()

func _ready():
	# We can't onready 'cause this is (still) loaded by the god singleton
	# and it'll crash custom scenes, such as the benchmarks.
#	if has_node("/root/App/Game/World/PickupsLayer"):
#		pickups_layer = get_node("/root/App/Game/World/PickupsLayer")
	print("Game board is ready.")

func _hashTile(tile):
	return tile
	# Vector2 is faster than a string hash, see benchmarks
#	return "%d %d" % [tile.x, tile.y]


### PICKLING ###################################################################

static func from_pickle(god, rick):
	var board = new(
		god,
		rick.hex_lattice_size
	)
	god.board = board
#	god.game.board = board
	
	for cell in rick.pickups_on_grid:
		var pickulpe = rick.pickups_on_grid[cell]
		var pickup_type = pickulpe.pickup_type
		var stack = pickulpe.stack
		var pickup = god.spawn_pickup(pickup_type, stack)
		pickup.position = pickulpe.position
		board.drop_on_grid(pickup, pickup.position)
	
	var fixtures_to_add = []
	for tile in rick.fixtures_lattice:
		var fixturles = rick.fixtures_lattice[tile]
		for fixturle in fixturles:
			var fixture_type = fixturle.fixture_type
			var fixture_config = god.fixtures[fixture_type]
			var fixture = fixture_config.script.from_pickle(god, fixturle)
			fixtures_to_add.append({
				'fixture': fixture,
				'layer': god.get_layer(fixture_config.layer),
			})
			board.add_tile_fixture(tile, fixture)
	
	# We want to add the fixtures to the scene only once all of them
	# have been added to the board, so their respective _ready calls can use it.
	for fixture_to_add in fixtures_to_add:
		fixture_to_add.layer.add_child(fixture_to_add.fixture)
	
	for tile in rick.grounds_lattice:
		var grounds_on_tile = rick.grounds_lattice[tile]
		for ground in grounds_on_tile:
			board.add_tile_ground(ground)
	
	return board

func to_pickle():
	var pickled_pog = {}
	for cell in self.pickups_on_grid:
		pickled_pog[cell] = self.pickups_on_grid[cell].to_pickle()
	
	var pickled_fol = {}
	for tile in self.fixtures_lattice:
		pickled_fol[tile] = []
		for fixture in self.fixtures_lattice[tile]:
			pickled_fol[tile].append(fixture.to_pickle())
	
	var pickled_gol = {}
	for tile in self.grounds_lattice:
		pickled_gol[tile] = []
		for ground in self.grounds_lattice[tile]:
			pickled_gol[tile].append(ground)
	
	return {
		'hex_lattice_size':  hex_lattice_size,
		'pickups_on_grid':   pickled_pog,
		'fixtures_lattice':  pickled_fol,
		'grounds_lattice':   pickled_gol,
	}


### LAYERS POISONED SUGAR ######################################################

func get_pickups_layer():
	if god.get_pickups_layer():
		return god.get_pickups_layer()
	assert not "any pickup layer to grab"


### GENERAL API ################################################################

func try_drop(pickup, xy, inventory=null):
	if can_drop(pickup, xy):
		do_drop(pickup, xy, inventory)
		return true
	else:
		return false

func can_drop(pickup, xy):
	var tile = pix_to_hex(xy)
	if has_tile_fixtures(tile):
		var tile_fixtures = get_tile_fixtures(tile)
		for tile_fixture in tile_fixtures:
			if tile_fixture.can_feed_pickup(pickup, xy):
				return true
	elif can_drop_on_grid(xy):
		return true
	# can't drop on fixture's ground, add exceptions here
	return false

func do_drop(pickup, xy, inventory=null):
	var tile = pix_to_hex(xy)

	if has_tile_fixtures(tile):
		var tile_fixtures = get_tile_fixtures(tile)
		for tile_fixture in tile_fixtures:
			if tile_fixture.can_feed_pickup(pickup, xy):
				remove_from_grid_if_on_it(pickup)
				remove_from_inventory_if_any(pickup, inventory)
				tile_fixture.feed_pickup(pickup, xy)
				return

	elif can_drop_on_grid(xy):
		remove_from_inventory_if_any(pickup, inventory)
		drop_on_grid(pickup, xy)
		return

	print("TRIED TO DROP BUT ACTUALLY CAN'T")
	assert 1 != 0

#func can_grab()

### FIXTURES ON HEX TILES ######################################################

func has_tile_fixtures(tile):
	return fixtures_lattice.has(_hashTile(tile))

func get_tile_fixture(tile):
	var f = get_tile_fixtures(tile)
	if f:
		return f[0]
	else:
		return null

func get_tile_fixtures(tile):
	if fixtures_lattice.has(_hashTile(tile)):
		return fixtures_lattice[_hashTile(tile)]
	else:
		return []

func add_tile_fixture(tile, fixture):
	if not fixtures_lattice.has(_hashTile(tile)):
		fixtures_lattice[_hashTile(tile)] = []
	
	fixture.hex_position = tile
	fixture.position = hex_to_pix(tile)
	var fixture_tiles = fixture.get_hex_positions()
	for fixture_tile in fixture_tiles:
		fixtures_lattice[_hashTile(fixture_tile)].append(fixture)

func remove_tile_fixture(fixture):
	for p in fixture.get_hex_positions():
		assert fixtures_lattice.has(_hashTile(p))
		var found = false
		for i in range(fixtures_lattice[_hashTile(p)].size()):
			var f = fixtures_lattice[_hashTile(p)][i]
			if f == fixture:
				found = true
				fixtures_lattice[_hashTile(p)].remove(i)
				break
		assert found
		if fixtures_lattice[_hashTile(p)].empty():
			fixtures_lattice.erase(_hashTile(p))

func get_tiles(): # get_occupied_tiles
	var tiles = []
	for k in fixtures_lattice:
		tiles.append(fixtures_lattice[k][0].hex_position)
	return tiles

func get_belt(tile):
	var tile_fixtures = get_tile_fixtures(tile)
	for fixture in tile_fixtures:
		if fixture is god.fixtures.belt.script:
			return fixture
	return null

func get_fixtures_in_circle(around_xy, in_range_xy):
	var around_tile = pix_to_hex(around_xy)
	# var hex_range = pix_to_hex(Vector2(in_range_xy,0))[0]
	# Probably faster to do an euclidian div like below
	var hex_range = int(in_range_xy) / int(SQRT_3 * hex_lattice_size)
	
	if hex_range > 2:
		assert not "free what you are doing. It has a cost !"
	
	var fixtures = []
	for fixture in get_tile_fixtures(around_tile):
		fixtures.append(fixture)
	for tile in get_tiles_in_circle(around_tile, hex_range):
		for fixture in get_tile_fixtures(tile):
			fixtures.append(fixture)
	
	return fixtures

func get_tiles_in_circle(around_tile, hex_range): # NOT REALLY IMPLEMENTED
	# https://www.redblobgames.com/grids/hexagons/#range-coordinate
#	assert not "left as is, please code me"
	return get_tiles_around(around_tile)


### GROUND ON HEX TILES ########################################################

func add_tile_ground(ground):
	var tile = ground.hex_position
	if not grounds_lattice.has(_hashTile(tile)):
		grounds_lattice[_hashTile(tile)] = []
	
	grounds_lattice[_hashTile(tile)].append(ground)


### HEX TILES UTILS ############################################################

static func rotate_positions_by_direction(positions, direction):
	"""
	Rotate the positions around (0,0).
	"""
	var tau_6th = HEX_DIRECTIONS.find(direction)
	assert tau_6th > -1
	
	var rotated_positions = []
	for p in positions:
		rotated_positions.append(rotate_position(p, tau_6th))
	return rotated_positions

static func rotate_position(position, circle_6th):
	var x = position[0]
	var y = position[1]
	var z = -x-y

	match circle_6th:
		0:
			return position
		1:
			return Vector2(-y, -z)
		2:
			return Vector2(z, x)
		3:
			return Vector2(-x, -y)
		4:
			return Vector2(y, z)
		5:
			return Vector2(-z, -x)
	assert not "supported, please gimme circle_6th between 0 and 5."


### GRID CELLS #################################################################

func pix_to_cell(xy):
	return xy.snapped(PICKUP_GRID_RES)
	#benchmark
#	return Vector2(
#		round((1.0*vec2.x*god.PICKUP_GRID_RES/god.PICKUP_RADIUS)),
#		round((1.0*vec2.y*god.PICKUP_GRID_RES/god.PICKUP_RADIUS))
#	)

func get_all_pickups_on_grid():
	return pickups_on_grid.values()

func get_pickups_on_grid_in_circle(xy_center, pix_radius):
	var radius_2 = pow(pix_radius, 2)
	var cell = pix_to_cell(xy_center)
	var cell_min = pix_to_cell(xy_center - Vector2(pix_radius, pix_radius))
	var cell_max = pix_to_cell(xy_center + Vector2(pix_radius, pix_radius))
	var pickups = []
	for i in range(cell_min[0], cell_max[0]+1):
		for j in range(cell_min[1], cell_max[1]+1):
			var k = Vector2(i,j)
			if pickups_on_grid.has(k):
				var pickup = pickups_on_grid[k]
				if (pickup.position - xy_center).length_squared() <= radius_2:
					pickups.append(pickup)
	return pickups

func can_drop_on_grid(xy):
	var cell = pix_to_cell(xy)
	var offset_x = ceil(PICKUP_GRID_RES[0]*3/2.0)
	var offset_y = ceil(PICKUP_GRID_RES[1]*3/2.0)
	for i in range(cell.x-offset_x, cell.x+offset_x):
		for j in range(cell.y-offset_y, cell.y+offset_y):
			if pickups_on_grid.has(Vector2(i,j)):
				return false
	return true

func drop_on_grid(pickup, xy):
	assert not pickups_on_grid.has(pix_to_cell(xy))
	pickup.position = xy
	pickups_on_grid[pix_to_cell(pickup.position)] = pickup
	if pickup.get_parent() != god.get_pickups_layer():
		if pickup.get_parent():
			pickup.get_parent().remove_child(pickup)
		god.get_pickups_layer().add_child(pickup)

#	pickup_dropped(pickup)

func remove_from_inventory_if_any(pickup, inventory):
	if inventory:
		assert pickup in inventory
		inventory.remove(inventory.find(pickup))

func remove_from_grid_if_on_it(pickup):
	forget_pickup(pickup)

func forget_pickup(pickup):  # deprecated, use grid methods
	var cell = pix_to_cell(pickup.position)
	if pickups_on_grid.has(cell):
		if pickup == pickups_on_grid[cell]:
			pickups_on_grid.erase(cell)


### GEOMETRY ###################################################################

func hex_to_pix(vec2):
	return HexagonalLattice.hex_to_pix(vec2, hex_lattice_size)

func pix_to_hex(vec2):
	"""
	Returns the *origin* of the closest hexagon tile (it's always integers)
	"""
	return HexagonalLattice.pix_to_hex(vec2, hex_lattice_size)

func get_tiles_around(tile):
	"""
	From 3 o'clock, clockwise.
	"""
	var around = []
	for direction in HEX_DIRECTIONS:
		around.append(tile + direction)
	return around

func get_hexs_in_rect(rect):
	"""
	Rect2 rect : in pixel land
	"""
	var hexs = []
	var top_left = rect.position
	var dimensions = rect.size
	
	var tl_hex = pix_to_hex(top_left)
	var bl_hex = pix_to_hex(Vector2(top_left[0], top_left[1] + dimensions[1]))
	var br_hex = pix_to_hex(top_left + dimensions)
	var tr_hex = pix_to_hex(Vector2(top_left[0] + dimensions[0], top_left[1]))
	
	# This dumb iteration gets too many hexagons
	for i in range(bl_hex[0], tr_hex[0]+1):
		for j in range(tl_hex[1], br_hex[1]+1):
			var possible_hex = Vector2(i,j)
			# … so we filter them, at great cost
			if rect.has_point(hex_to_pix(possible_hex)):
				hexs.append(Vector2(i,j))
	
	return hexs
	

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
