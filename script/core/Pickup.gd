extends Sprite



#var x setget set_x,get_x
#func set_x(x):
#	icon.position.x = x
#func get_x():
#	return icon.position.x
#
#var y setget set_y,get_y
#func set_y(y):
#	icon.position.y = y
#func get_y():
#	return icon.position.y

var pickup_type = "undefinium"
var display_name = "<փ⋅¿"  # displayed name in-game

var stack = 1  # current stack value
var pickable = true

var god      # our global configuration
var entropy  # incrementing integer or null (kind of deprecated)
var config   # config dict from god

# The (accelerating) velocity of this pickup when grabbed. See Character.
var _grabbed_vel = 0


func init(god, config, stack, entropy=0):
	"""
	We're using init instead of _init because _init inheritance is automatic
	and it does not accept arguments. But you can override this in pickups defs.
	So that means all pickups MUST be instantiated by either
		- god.spawn_pickup() or
		- Pickup.from_pickle()
	whose reponsibility is to handle calling this init() method.
	
	entropy: Integer
		An incrementing integer (god handles that). Less than 1<<30.
	"""
	self.god = god
	self.entropy = entropy
	self.stack = stack
	self.config = config
	self.pickup_type = config.slug
	self.display_name = config.name
#	self.display_name = ("%s" % config.name)
#	self.display_name = tr("%s_name" % config.slug)
	# Node properties
	self.name = "%s#%09d" % [config.slug, entropy]
	# Sprite properties
	self.texture = config.texture
	self.scale = Vector2(0.5, 0.5)


### PICKLING ###################################################################

static func from_pickle(god, pickle):
	var pickup_type = pickle.pickup_type
	assert god.pickups.has(pickup_type)
	var config = god.pickups[pickup_type]
	var pickup = config.script.new()
	pickup.init(god, config, pickle.stack, god.spawned_pickups_count)
	god.increment_spawned_pickups_count()
	if pickle.has('name'):
		pickup.name = pickle.name
	if pickle.has('position'):
		pickup.position = pickle.position
	if pickle.has('pickable'):
		pickup.pickable = pickle.pickable
	
	return pickup

func to_pickle():
	return {
		'pickup_type': pickup_type,
		'stack':       stack,
		'name':        name,
		'position':    position,
		'pickable':    pickable,
	}


### GENERAL API ################################################################

func is_of_type(pickup_type):
	return self.pickup_type == pickup_type

func is_of_same_type_as(other_pickup):
	return other_pickup.pickup_type == self.pickup_type

func is_fully_stacked():
	return self.stack >= god.pickups[pickup_type].stack

func get_room_left_in_stack():
	return self.config.stack - self.stack

func is_fixture(fixture_type=null):
	if not self.config.has('fixture'):
		return false
	elif null == fixture_type:
		return true
	else:
		return self.config.fixture == fixture_type


### CHEATER"S API ##############################################################

func fill_stack():
	self.stack = self.config.stack

