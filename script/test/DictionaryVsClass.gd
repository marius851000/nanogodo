extends Node

# Benchmarking Classes vs Dicts linux x64 i7-4790k
# TIMER Instanciation
#  total: 2801 ms
#  lap 1 : 1859 ms (197%) Memory Usage: 1224
#  lap 2 : 942 ms (100%) Memory Usage: 792
# TIMER Reading
#  total: 637 ms
#  lap 1 : 172 ms (100%) Memory Usage: 0
#  lap 2 : 465 ms (270%) Memory Usage: 360
# TIMER Writing
#  total: 538 ms
#  lap 1 : 113 ms (100%) Memory Usage: 0
#  lap 2 : 425 ms (376%) Memory Usage: 360

#Benchmarking Classes vs Dicts Win7 x64 Phenom II 1090T
#TIMER Instanciation
#  total: 5929 ms
#  lap 1 : 4240 ms (251%) Memory Usage: 1224
#  lap 2 : 1689 ms (100%) Memory Usage: 792
#TIMER Reading
#  total: 1528 ms
#  lap 1 : 440 ms (100%) Memory Usage: 0
#  lap 2 : 1088 ms (247%) Memory Usage: 360
#TIMER Writing
#  total: 1421 ms
#  lap 1 : 319 ms (100%) Memory Usage: 0
#  lap 2 : 1102 ms (345%) Memory Usage: 360


class Pickup:
	var type
	var position
	var pickable
	func _init(type, position, pickable):
		self.type = type
		self.position = position
		self.pickable = pickable

func _ready():
	print("Benchmarking Classes vs Dicts")
	
#	var b = {'a':Vector2(1,-1), 'b': "test"}
#	var a = var2str(b)
#	print(a)
#	print(str2var(a))
#	assert a == var2str(str2var(a))

#	var a = str2var(var2str(Vector2(1,-1)))
#	print(Vector2(1,1) + a * 2)
	
	start_timer("Instanciation")
	create_classes()
	register_lap()
	create_dicts()
	register_lap()
	close_timer()
	
	start_timer("Reading")
	read_classes()
	register_lap()
	read_dicts()
	register_lap()
	close_timer()
	
	start_timer("Writing")
	write_classes()
	register_lap()
	write_dicts()
	register_lap()
	close_timer()
	
################################################################################

var iterations = 1000000
var last_class
var last_dict

func create_classes():
	for i in range(iterations):
		last_class = Pickup.new('plop', Vector2(1,0), true)

func create_dicts():
	for i in range(iterations):
		last_dict = {'type':'plop', 'position':Vector2(1,0), 'pickable':true}

func read_classes():
	for i in range(iterations):
		last_class.type
		last_class.position
		last_class.pickable

func read_dicts():
	for i in range(iterations):
		last_dict.type
		last_dict.position
		last_dict.pickable

func write_classes():
	for i in range(iterations):
		last_class.type = 'plooop'
		last_class.position = Vector2(1,0)
		last_class.pickable = false

func write_dicts():
	for i in range(iterations):
		last_dict.type = 'plooop'
		last_dict.position = Vector2(1,0)
		last_dict.pickable = false


### TIMER ######################################################################

var timer_name
var timer_start
var timer_laps
func start_timer(name="SOMETHING"):
	timer_name = name
	timer_laps = []
	timer_start = {
		'time':OS.get_ticks_msec(),
		'usage':OS.get_static_memory_usage()
	}
func register_lap():
	timer_laps.append({
		'time': OS.get_ticks_msec(),
		'usage': OS.get_static_memory_usage()
	})
func close_timer():
	var total = OS.get_ticks_msec() - timer_start.time
	assert total > 0
	var laps = []
	var best_lap_time = total
	print("TIMER %s" % timer_name)
	print("  total: %d ms" % total)
	var prev_lap = timer_start
	for i in range(timer_laps.size()):
		var lap_time = timer_laps[i].time - prev_lap.time
		assert lap_time > 0
		laps.append({
			'time': lap_time,
			'usage': timer_laps[i].usage - prev_lap.usage
		})
		if lap_time < best_lap_time:
			best_lap_time = lap_time
		prev_lap = timer_laps[i]
	for i in range(laps.size()):
		print("  lap %d : %d ms (%d%%) Memory Usage: %d" % [
			i+1, laps[i].time, round(laps[i].time / (0.01 * best_lap_time)),
			laps[i].usage
		])
