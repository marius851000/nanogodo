extends Node

const GameBoard = preload("res://script/core/GameBoard.gd")
const Pickup = preload("res://script/core/Pickup.gd")
const Character = preload("res://script/core/Character.gd")
const GameOverOverlay = preload("res://script/gui/GameOverOverlay.gd")
const DevLevel = preload("res://script/level/DevLevel.gd")
const GameMakerLevel = preload("res://script/level/GameMakerLevel.gd")


const MenuPath = preload("res://scene/Menu.tscn")
const WorldTree = preload("res://scene/World.tscn")

var god
var app
var board

var level # Instance of Level
var level_slug
var campaign_slug

# Local main character
var character

# So we can repaint the ground. This is pickled.
# This variable will be useless in an open world.
var ground_tiles = Array() # of tiles ie. Vector2(q,r)

var world_node
var menu_node

var should_generate_world = true

################################################################################

var ms_at_start = OS.get_ticks_msec()
var game_duration = 0.0  # in s



func _init(god, app, campaign_slug=null, level_slug=null):
	"""
	campaign: String
		A key of god.campaigns
	level: String
		A key of a level in god.campaigns[campaign].levels
	"""
	randomize() # let's shake that booty
	self.name = "Game" # don't change this, we use it a lot in paths
	self.god = god
	self.app = app
	self.campaign_slug = campaign_slug
	self.level_slug = level_slug
	if null != campaign_slug and null != level_slug:
		self.level = god.campaigns[campaign_slug] \
		                .levels[level_slug].script.new()
		
		#self.level = DevLevel.new()  # debug toggle
		#self.level = GameMakerLevel.new()  # debug toggle
		
		self.level.init(god, self)
		add_child(self.level)

	create_world_layers()
	create_game_board()
	create_local_character()


func _ready():
	god.game = self # shouldn't we move this to init ?
	app.unload_main_menu()
	set_game_board(board)
	set_local_character(character)
	
	if self.should_generate_world:
		assert self.level
		self.level.create_world()
#		generate_level_dev()
		self.should_generate_world = false
	else:
		create_ground_texture()

	start_machinery()
	app.pause_able = true

	# Initially display the game objective
	character.open_quests_ui()

	#game_over() # debug helper toggle


var _last_game_over_check = OS.get_ticks_msec()
func _process(delta):
	var now = OS.get_ticks_msec()
	self.game_duration += delta
	if now > _last_game_over_check + 10000: # check every 10 seconds
		_last_game_over_check = now
		if self.level.is_complete():
			self.game_over()


### PICKLING ###################################################################

static func from_pickle(god, app, rick):
	var game = new(god, app)
	game.character.queue_free()
	game.board.queue_free()
	game.character = Character.from_pickle(god, rick.character) 
	game.board = GameBoard.from_pickle(god, rick.board)
	if rick.has('game_duration'):
		game.game_duration = rick.game_duration
	if rick.has('level'):
		assert rick.has('level_slug') and rick.has('campaign_slug')
		game.campaign_slug = rick.campaign_slug
		game.level_slug = rick.level_slug
		assert god.campaigns.has(game.campaign_slug)
		assert god.campaigns[game.campaign_slug].levels.has(game.level_slug)
		game.level = god.campaigns[game.campaign_slug]  \
		                .levels[game.level_slug].script  \
		                .from_pickle(god, game, rick.level)
	if rick.has('ground_tiles'):
		game.ground_tiles = rick['ground_tiles']
	if rick.has('should_generate_world'):
		game.should_generate_world = rick['should_generate_world']
	return game

func to_pickle():
	return {
		'character':             character.to_pickle(),
		'board':                 board.to_pickle(),
		'ground_tiles':          self.ground_tiles,
		'should_generate_world': self.should_generate_world,
		'level':                 self.level.to_pickle(),
		'campaign_slug':         self.campaign_slug,
		'level_slug':            self.level_slug,
		'game_duration':         self.game_duration,
	}


### GAME BOARD #################################################################

func create_game_board():
	var hex_lattice_size = 36
	
	board = GameBoard.new(god, hex_lattice_size)
#	print(String(board.get_instance_id()))

func set_game_board(board):
	god.board = board
	add_child(board)


### CHARACTER ##################################################################

func create_local_character():
	character = Character.new(god)
	character.position = Vector2(125, 15)

func set_local_character(character):
	god.get_general_layer().add_child(character)


### LAYERS #####################################################################

func create_world_layers():
	world_node = WorldTree.instance()
	god.register_layer("General", world_node.get_node("GeneralLayer"))
	god.register_layer("Pickups", world_node.get_node("PickupsLayer"))
	god.register_layer("Cursor", world_node.get_node("CursorLayer"))
	god.register_layer("FixedHud", world_node.get_node("FixedHudLayer"))
	god.register_layer("Hud", world_node.get_node("HudLayer"))
	god.register_layer("InteractiveHud", world_node.get_node("InteractiveHudLayer"))
	god.register_layer("Belts", world_node.get_node("BeltsLayer"))
	god.register_layer("Earth", world_node.get_node("EarthLayer"))
	god.register_layer("Feedback", world_node.get_node("FeedbackLayer"))
	add_child(world_node)


### MENUS ######################################################################

func pause_game():
	if app.pause_able:
		app.pause_able = false
		world_node = $World
		menu_node = MenuPath.instance()
		get_parent()._capture_game_to_img()
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
		remove_child(world_node)
		
		add_child(menu_node)
		menu_node._set_capture_texture()
		app.pause_able = true
	
func unpause_game(): # resume_game
	if app.pause_able:
		app.pause_able = false
		destroy_menu()
		add_child(world_node)
		app.pause_able = true

func destroy_menu():
	if has_node("Menu"):
		$Menu.queue_free()
	if menu_node:
		menu_node.queue_free()


### GAME OVER ##################################################################

func game_over():
	var goo = GameOverOverlay.new(god, app, self)
	god.get_fixed_hud_layer().add_child(goo)
#	goo.show_modal(true)


### MAP GENERATION #############################################################

func create_ground_texture():
	for tile in self.board.grounds_lattice:
		add_ground_polygon_to_tile(tile)

var ground_polygons_lattice = {}
# This is super ugly, super laggy, but I'm not sure how to proceed.
func add_ground_polygon_to_tile(tile, color=null):
	assert not self.ground_polygons_lattice.has(tile)
	var s = 0.91
	var w = 0.73
	var two_colors = [Color(w,w,w), Color(s,s,s)]
	
	var rdm = round(rand_range(0,42))
	var rdm_2 = rand_range(0,100)
	
	var vertices = []
	var uvs = [] # later
	var colors = []
	for i in range(god.board.HEX_DIRECTIONS.size()):
		var dd = (god.board.HEX_DIRECTIONS[i]+god.board.HEX_DIRECTIONS[(i+1)%6])
		dd = dd * 0.25 * 1.1547 * 1.125
#		if rdm == 0:
#			dd = dd * (1-0.001 * rdm_2)
		vertices.append(god.board.hex_to_pix(tile+dd))
		colors.append(two_colors[i%2])

	var polygon = Polygon2D.new()
	polygon.name = "GroundPolygon%s" % tile
	polygon.polygon = vertices
	polygon.vertex_colors = colors
	
	self.ground_polygons_lattice[tile] = polygon
	god.get_earth_layer().get_node("GroundPolygonsLayer").add_child(polygon)

func clear_ground_polygon_of_tile(tile):
	assert self.ground_polygons_lattice.has(tile)
	var polygon = self.ground_polygons_lattice[tile]
	self.ground_polygons_lattice.erase(tile)
	polygon.get_parent().remove_child(polygon)
	polygon.queue_free()

### MACHINERY TICKS ############################################################

func start_machinery():
	var belts_animation = god.get_belts_layer().get_node("BeltsAnimation")
	belts_animation.play("ConveyorAnimation", -1, 0.25)

# Our spritesheet images have 16 frames, so `frame` ranges from 0 to 15.
const MACHINERY_TICKS = 16

# The current machinery tick
# May become negative after overflow. Would take a LONG time, though.
# ~10 years at 15 ticks per second.
var current_machinery_tick = 0

var previous_frame_was_zero_already = false
#var last = null
func on_machinery_tick(frame):
	# frame = 0 is rapidly fired twice in a row because of the track loop hack
	if 0 == frame and previous_frame_was_zero_already:
		previous_frame_was_zero_already = false
		return # skip the micro-beat
	if 0 == frame:
		previous_frame_was_zero_already = true
	
#	if not last:
#		last = OS.get_ticks_msec()
#		return
#	var mt = OS.get_ticks_msec() - last
#	print("%02d %d"%[frame,mt])

	var progress = frame / (MACHINERY_TICKS - 1)  # sugary value between 0 and 1
	for node in god.get_belts_layer().get_children():
#		if node.has_method("on_machinery_tick"):  # hmmm… benchmark!?!
		if node is god.fixtures.belt.script:
			node.frame = frame
			node.on_machinery_tick(progress, current_machinery_tick)

	for node in god.get_general_layer().get_children():
#		if node.has_method("on_machinery_tick"):  # hmmm… benchmark!?!
		if node is god.fixtures.inserter.script:
			node.on_machinery_tick(progress)
		elif node is god.fixtures.pickup_spawner.script:
			node.on_machinery_tick(progress)
		elif node is god.fixtures.pickup_despawner.script:
			node.on_machinery_tick(progress)
		elif node is god.fixtures.factory.script:
			node.on_machinery_tick(progress)
	
	# Loop to 0 on overflow ; may be very bad design ; may not. Hmmm…
	#current_machinery_tick = max(0, current_machinery_tick + 1)
	current_machinery_tick += 1







