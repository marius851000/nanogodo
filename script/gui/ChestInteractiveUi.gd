#extends "res://script/gui/WindowBasedContainer.gd"
extends CenterContainer

const InteractiveInventory = preload("res://script/gui/InteractiveInventoryGui.gd")
const CLOSE_BUTTON_NORMAL  = preload("res://data/base/graphics/ui/panel_cross_stylebox_normal.tres")
const CLOSE_BUTTON_HOVER   = preload("res://data/base/graphics/ui/panel_cross_stylebox_hover.tres")
const CLOSE_BUTTON_PRESSED = preload("res://data/base/graphics/ui/panel_cross_stylebox_pressed.tres")

var iui
var vbox
var label

var god
var fixture
var character
var hand

func _init(god, fixture, title, inventory):
	assert god
	assert god.game
	assert god.game.character
	self.god = god
	self.fixture = fixture
	self.character = god.game.character
	self.hand = self.character.hand
	
#	self.mfv_anchor_top		= 0.5
#	self.mfv_anchor_bottom	= 0.5
#	self.mfv_anchor_left	= 0.5
#	self.mfv_anchor_right	= 0.5

	self.theme = preload("res://data/base/graphics/ui/hud_theme.tres")

	self.anchor_top = 0
	self.anchor_left = 0
	self.anchor_right = 1
	self.anchor_bottom = 1
	self.margin_top = -100
#	self.mouse_filter = MOUSE_FILTER_IGNORE
	
	iui = InteractiveInventory.new(god, self.character, inventory)
	var iui_size = iui.rect_min_size  # we could also use this
	
	vbox = VBoxContainer.new()
	
	var background = Panel.new()
	background.anchor_top = 0
	background.anchor_left = 0
	background.anchor_right = 1
	background.anchor_bottom = 1
	background.margin_top = -5
	background.margin_bottom = 4
	background.show_behind_parent = true
	
#	var header = Container.new()
#	background.anchor_top = 0
#	background.anchor_left = 0
#	background.anchor_right = 1
#	background.anchor_bottom = 1
	
	var close_side = 16
	var close = Button.new()
	close.anchor_top = 0
	close.anchor_left = 1
	close.anchor_right = 1
	close.anchor_bottom = 0
	close.margin_top = -2
	close.margin_left = -1 * (close_side + 3)
	close.margin_right = -1 * (close_side + 3)
	close.margin_bottom = -2
	close.rect_min_size = Vector2(16, 16)
	close.focus_mode = Control.FOCUS_NONE
	close.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
	close.add_stylebox_override("normal", CLOSE_BUTTON_NORMAL)
	close.add_stylebox_override("hover", CLOSE_BUTTON_HOVER)
	close.add_stylebox_override("pressed", CLOSE_BUTTON_PRESSED)
	close.connect("pressed", self, "on_close_pressed", [])
	
	var label = Label.new()
	label.text = "  %s" % title
	label.mouse_filter = Control.MOUSE_FILTER_IGNORE  # close btn under label
	
	vbox.add_child(label)
	vbox.add_child(iui)
	
	label.add_child(background)
	label.add_child(close)
	add_child(vbox)
	
	update_display()

func on_close_pressed():
	fixture.close_interactive_ui()

func close():
	get_parent().remove_child(self)
	self.queue_free()

func _gui_input(event):
	if event is InputEventMouseButton:
		fixture.close_interactive_ui()

#func _ready():
#	print(vbox.rect_size)  # it's ok here
	
## Process is used to animate the hand. Should be pretty cheap.
#var current_milliframe = 0
#var animation_speed = 333 # of the slot hand, in milliframes per process
#func _process(delta):
#	var slot = self.character.hand.held_inventory_slot
#	if slot and self.character.hand.held_inventory == inventory:
#		var hand_sprite = slots[slot].get_node(SLOT_ICON_NODE_NAME)
#		current_milliframe += animation_speed
#		current_milliframe %= 1000 * SLOT_HAND_SPRITE_HFRAMES
#		hand_sprite.frame = floor(current_milliframe / 1000.0)


## ACTIONS #####################################################################

func update_display(all_of_them=true):
	iui.update_display()

