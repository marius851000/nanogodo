#extends "res://script/gui/WindowBasedContainer.gd"
extends CenterContainer

const InteractiveInventory = preload("res://script/gui/InteractiveInventoryGui.gd")
const RecipeSelectionGui = preload("res://script/gui/RecipeSelectionGui.gd")
const CLOSE_BUTTON_NORMAL  = preload("res://data/base/graphics/ui/panel_cross_stylebox_normal.tres")
const CLOSE_BUTTON_HOVER   = preload("res://data/base/graphics/ui/panel_cross_stylebox_hover.tres")
const CLOSE_BUTTON_PRESSED = preload("res://data/base/graphics/ui/panel_cross_stylebox_pressed.tres")

var rs
var vbox
var hbox_intake
var hbox_outake
var label
var in_iuis = Array()
var out_iuis = Array()
var progress_bar

var god
var fixture
var character
var hand

func _init(god, fixture, title, available_recipes, intake_inventories, outake_inventories):
	assert god
	assert god.game
	assert god.game.character
	self.god = god
	self.fixture = fixture
	self.character = god.game.character
	self.hand = self.character.hand
	
#	self.mfv_anchor_top		= 0.5
#	self.mfv_anchor_bottom	= 0.5
#	self.mfv_anchor_left	= 0.5
#	self.mfv_anchor_right	= 0.5

	self.theme = preload("res://data/base/graphics/ui/hud_theme.tres")

	self.anchor_top = 0
	self.anchor_left = 0
	self.anchor_right = 1
	self.anchor_bottom = 1
	self.margin_top = -100
#	self.mouse_filter = MOUSE_FILTER_IGNORE
	
	rs = RecipeSelectionGui.new(god, available_recipes, null, true)
	rs.connect("slot_pressed", self, "on_recipe_slot_pressed", [])
	#var rs_size = rs.rect_min_size  # we could also use this
	
	hbox_intake = HBoxContainer.new()
	hbox_outake = HBoxContainer.new()
	vbox = VBoxContainer.new()
	progress_bar = ProgressBar.new()
	
	update_iotakes(intake_inventories, outake_inventories)
#	var i = 0
#	for intake_inventory in intake_inventories:
#		in_iuis.append(InteractiveInventory.new(god, self.character, intake_inventory))
#		hbox_intake.add_child(in_iuis[i])
#		i += 1
#	i = 0 
#	for outake_inventory in outake_inventories:
#		out_iuis.append(InteractiveInventory.new(god, self.character, outake_inventory))
#		hbox_outake.add_child(out_iuis[i])
#		i += 1
	
	var background = Panel.new()
	background.anchor_top = 0
	background.anchor_left = 0
	background.anchor_right = 1
	background.anchor_bottom = 1
	background.margin_top = -5
	background.margin_bottom = 4
	background.show_behind_parent = true
	
	# How to remove this?
	background.rect_min_size = Vector2(220, 190)
	
#	var header = Container.new()
#	background.anchor_top = 0
#	background.anchor_left = 0
#	background.anchor_right = 1
#	background.anchor_bottom = 1
	
	add_child(background)
	
	vbox.add_child(hbox_intake)
	vbox.add_child(rs)
	vbox.add_child(progress_bar)
	vbox.add_child(hbox_outake)

#	background.add_child(vbox)
	add_child(vbox)
	
	update_display()

#func _ready():
#	print(vbox.rect_size)  # it's ok here

func on_recipe_slot_pressed(slot_node, recipe_slug, action_type):
	unhook_hand()
	self.character.hand.clear(false)
	assert self.character.hand.is_empty()
	var pickups = self.fixture.get_all_pickups()
	
	if self.character.has_room_for_pickups(pickups):
		self.character.store_all(pickups)
		for slot in self.rs.slots.values():
			if slot == slot_node:
				if not slot.pressed:
					self.fixture.forget_recipe_type()
				else:
					self.fixture.set_recipe_type(recipe_slug)
#					slot.pressed = true
				self.rs.update_display()
			else:
				slot.pressed = false
	update_recipe_slots()

func update_recipe_slots():
	assert self.rs
	for slot in self.rs.slots.values():
		if self.fixture.get_recipe_type() != slot.recipe_slug:
			slot.pressed = false
		else:
			slot.pressed = true

func on_close_pressed():
	fixture.close_interactive_ui()

func close():
	get_parent().remove_child(self)
	self.queue_free()

func _gui_input(event):
	if event is InputEventMouseButton:
		fixture.close_interactive_ui()

func update_iotakes(intake_inventories, outake_inventories):
	for child in hbox_intake.get_children():
		hbox_intake.remove_child(child)
		child.queue_free()
	for child in hbox_outake.get_children():
		hbox_outake.remove_child(child)
		child.queue_free()
	var i = 0
	in_iuis = Array()
	for intake_inventory in intake_inventories:
		in_iuis.append(InteractiveInventory.new(god, self.character, intake_inventory))
		hbox_intake.add_child(in_iuis[i])
		i += 1
	i = 0 
	out_iuis = Array()
	for outake_inventory in outake_inventories:
		out_iuis.append(InteractiveInventory.new(god, self.character, outake_inventory))
		hbox_outake.add_child(out_iuis[i])
		i += 1


## ACTIONS #####################################################################

func unhook_hand():
	for iui in self.in_iuis:
		if iui.inventory == hand.held_inventory:
			hand.clear()

func update_display():
	for in_iui in in_iuis:
		in_iui.update_display()
	
	for out_iui in out_iuis:
		out_iui.update_display()
	
	#print(fixture.get_craft_progress())
	progress_bar.value = fixture.get_craft_progress()*100
	rs.update_display(null)
	
	update_recipe_slots()

