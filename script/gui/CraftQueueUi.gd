extends Control # Container of some sort

const CraftQueueSlot = preload("res://script/gui/CraftQueueSlot.gd")

const MOVE_SPEED = 5.0

var god
var character
func _init(god, character):
	self.god = god
	self.character = character
	self.name = "CraftQueueUi"
	
	self.margin_left = 4
	self.margin_top = 4

var slots = Array() # of CraftQueueSlot

func add_to_queue(recipe_slug, amount):
	var slot = CraftQueueSlot.new(god, recipe_slug, amount)
	self.slots.append(slot)
	self.add_child(slot)
	slot.set_owner(self)

func remove_slot(slot_index):
	var slot = self.slots[slot_index]
	remove_child(slot)
	slot.queue_free()
	self.slots.remove(slot_index)

func update_display(progress=0.0):
	# Update the slots' displays
	for i in range(self.slots.size()):
		if 0 == i:
			self.slots[i].update_display(progress)
		else:
			self.slots[i].update_display()
	
	# Remove first slot if any and if stale
	if progress >= 1.0 and not self.slots.empty():
		remove_slot(0)
	
	# Re-position the slots
	for i in range(self.slots.size()):
		var slot = self.slots[i]
		# Inch towards where it should be (hence the animation)
		slot.margin_left += ((i * 34) - slot.margin_left) * 0.01 * MOVE_SPEED
		slot.margin_top += (0 - slot.margin_top) * 0.01 * MOVE_SPEED

func queue_free():
	slots.clear()
	slots = null
	.queue_free()
