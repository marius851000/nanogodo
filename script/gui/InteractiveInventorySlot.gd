extends Button


signal slot_pressed(slot, inventory, inventory_key, is_some, is_all)

var inventory
var inventory_key

func _init(inventory, inventory_key):
	self.inventory = inventory
	self.inventory_key = inventory_key
	self.enabled_focus_mode = false

func _ready():
	self.connect("mouse_entered", self, "on_mouse_entered")

func _pressed():
	var is_all = Input.is_action_pressed("action_select_modifier_all")
	var is_some = Input.is_action_pressed("action_select_modifier_some")
	emit_signal(
		"slot_pressed",
		self, self.inventory, self.inventory_key, is_some, is_all
	)

func on_mouse_entered():
	setup_tooltip()

func is_empty():
	return null == self.inventory.find_at(self.inventory_key)

func get_pickup():
	return self.inventory.find_at(self.inventory_key)

func retrieve_pickup():
	return self.inventory.retrieve_from_slot(self.inventory_key)

func setup_tooltip():
	if not is_empty():
		set_tooltip(get_pickup().display_name)
