extends CenterContainer

var god
var app
var game

func _init(god, app, game):
	self.god = god
	self.app = app
	self.game = game
	
	self.theme = preload("res://data/base/graphics/ui/hud_theme.tres")

	self.anchor_top = 0
	self.anchor_left = 0
	self.anchor_right = 1
	self.anchor_bottom = 1
	self.margin_top = -100
	
#	self.rect_min_size = Vector2(500,200)

	var vbox = VBoxContainer.new()
	
#	var background = NinePatchRect.new()
	
	var background = Panel.new()
	background.anchor_top = 0
	background.anchor_left = 0
	background.anchor_right = 1
	background.anchor_bottom = 1
	background.margin_top = -15
	background.margin_right = 16
	background.margin_bottom = 164
	background.margin_left = -16
	background.show_behind_parent = true
	
	var font = DynamicFont.new()
	font.font_data = preload("res://data/base/fonts/HEXAGON cup font BOLD.ttf")
	font.size = 42
	
	var label = Label.new()
	label.text = "MISSION  COMPLETE"
	label.add_font_override('font', font)
	label.add_color_override('font_color_shadow', Color(0,0,0))
	
	var msg = Label.new()
	msg.text = """
	Thanks to your insighted wisdom, your host will survive a little bit longer!
	
	This game was made in thirty days, as a challenge, by three Godot newbies.
	There is only one level for now, the one you just completed.
	Join us on gitlab to follow our progress, report bugs, and add more content!
	"""
	
	var quit_button = Button.new()
	quit_button.text = "I AM AWESOME"
	quit_button.margin_top = 300
	quit_button.rect_min_size = Vector2(0,50)
	quit_button.connect("pressed", self, "on_quit", [])
	
	label.add_child(background)
	vbox.add_child(label)
	vbox.add_child(msg)
	vbox.add_child(quit_button)
	add_child(vbox)


func on_quit():
	OS.shell_open(god.gitlab_url)
	app.start_main_menu()
	