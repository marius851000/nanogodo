extends Label

var god
var lifetime # s
var initial_position
func _init(god, text, lifetime=3.0):
	self.god = god
	self.text = text
	self.lifetime = lifetime
	self.initial_position = Vector2(-20,-80)
	self.rect_global_position = self.initial_position

var born_at # ms
var dies_at # ms
var fade_at # ms
func _ready():
	born_at = OS.get_ticks_msec()
	dies_at = born_at + 1000.0 * lifetime
	fade_at = born_at +  800.0 * lifetime
	initial_position = self.rect_global_position

var upwards = 0.0  # in thousandths of pixels, along -Y
func _process(delta):
	var now = OS.get_ticks_msec()
	upwards += delta * 13333.3
	if upwards > 1000.0:
		self.rect_global_position += Vector2(0, -1)
		upwards = 0
	# Stays fixed to the map
#	var upwards_offset = Vector2(0, -1 * floor(0.001 * upwards))
#	self.rect_global_position = self.initial_position + upwards_offset
	
	if now > dies_at:
		set_process(false)
		queue_free()
	elif now > fade_at:
		var alpha = (dies_at - now) / ((dies_at - fade_at) * 1.0) # * 1.0 … SIGH
		self.modulate = Color(1, 1, 1, alpha)
