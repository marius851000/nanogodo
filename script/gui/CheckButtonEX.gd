extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var disabled_theme = preload("res://data/base/graphics/ui/check_button_disabled.tres")
onready var enabled_theme = theme

func set_theme():
	if disabled:
		theme = disabled_theme
	else:
		theme = enabled_theme

func _ready():
	set_theme()

func _process(delta):
	set_theme()

