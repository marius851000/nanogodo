extends Control

const SLOT_LABEL_NODE_NAME = "CraftSlotLabel"
const SLOT_ICON_NODE_NAME = "CraftSlotIcon"

var god
var recipe
var recipe_slug
var amount
var rect_side = 30 # px

func _init(god, recipe_slug, amount=1):
	assert god
	assert recipe_slug
	assert amount > 0
	self.god = god
	self.recipe_slug = recipe_slug
	assert god.recipes.has(recipe_slug)
	self.recipe = god.recipes[recipe_slug]
	self.amount = amount
	
	# We want the initial position of the slot to be under the mouse
#	var tr = god.game.character.get_global_transform_with_canvas()
#	var tr = god.game.character.cam.get_canvas_transform()
	var tr = god.game.character.get_viewport_transform()
	var pos = god.game.character.get_global_mouse_position()
	#tr.xform(pos) # /!. Wrong documentation : Vector2 is returned
	pos = tr.xform(pos)
	
	self.margin_top = pos.y - (self.rect_side/2.0)
	self.margin_left = pos.x - (self.rect_side/2.0)
	
	create_tree_structure()

var slot_img
var slot_lbl
func create_tree_structure():
	assert get_child_count() == 0
	
	self.name = "CraftSlot%s" % self.recipe.slug
	self.theme = preload("res://data/base/graphics/ui/toolbelt_slot_style.tres")
	self.rect_min_size = Vector2(self.rect_side, self.rect_side)
	self.rect_clip_content = true
	
	var background = NinePatchRect.new()
	background.name = "IS_ADRENESIS_WATCHING?…YES"
	background.texture = preload("res://data/base/graphics/ui/panel.png")
	background.axis_stretch_horizontal = NinePatchRect.AXIS_STRETCH_MODE_TILE_FIT
	background.axis_stretch_vertical = NinePatchRect.AXIS_STRETCH_MODE_TILE_FIT
	background.patch_margin_left   = 5
	background.patch_margin_top    = 5
	background.patch_margin_right  = 5
	background.patch_margin_bottom = 5
	background.anchor_left   = 0
	background.anchor_top    = 0
	background.anchor_right  = 1
	background.anchor_bottom = 1
	background.show_behind_parent = true
	
	add_child(background)
	
	# Holds the recipe texture
	slot_img = Sprite.new()
	slot_img.name = SLOT_ICON_NODE_NAME
	slot_img.offset = Vector2(-1, -1)
	slot_img.centered = false
	slot_img.texture = self.recipe.texture
	add_child(slot_img)
	
	# Hold the amount of crafts left to complete
	slot_lbl = Label.new()
	slot_lbl.text = String(self.amount) if self.amount > 1 else ""
	slot_lbl.name = SLOT_LABEL_NODE_NAME
	slot_lbl.grow_vertical = Control.GROW_DIRECTION_BEGIN
	slot_lbl.grow_horizontal = Control.GROW_DIRECTION_BEGIN
	slot_lbl.align = Label.ALIGN_RIGHT
	slot_lbl.anchor_top = 0.97
	slot_lbl.anchor_left = 0.96
	add_child(slot_lbl)

func update_display(progress=0.0):
	if progress >= 1.0:
		slot_lbl.text = "<փ⋅"
	else:
		slot_lbl.text = String(floor(progress * 100)) + '%'

#func queue_free():
#	print("Slot queued for free.")
#	slot_img = null
#	slot_lbl = null
#	.queue_free()
#
### AAAAAARGH THIS IS NEVER CALLED BY QUEUE_FREE
#func free():
#	print("Slot freed.")
#	slot_img = null
#	slot_lbl = null
#	.free()
