extends Control

const buttons_name = [ "VIDEO", "AUDIO", "CONTROL", "GAME", "BACK" ]

onready var video_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/VideoSettings")
onready var audio_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/AudioSettings")
onready var control_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/ControlSettings")
onready var game_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/GameSettings")
onready var back_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/Back")

var menu_node
func set_menu_node(menu_node):
	self.menu_node = menu_node

func _ready():
	video_button.connect("pressed", self, "video_settings")
	audio_button.connect("pressed", self, "audio_settings")
	control_button.connect("pressed", self, "control_settings")
	game_button.connect("pressed", self, "game_settings")
	back_button.connect("pressed", self, "back")

	video_button.set_text(buttons_name[0])
	audio_button.set_text(buttons_name[1])
	control_button.set_text(buttons_name[2])
	game_button.set_text(buttons_name[3])
	back_button.set_text(buttons_name[4])
	pass


func video_settings():
	menu_node.load_video_settings_menu()
	pass

func audio_settings():
	menu_node.load_audio_settings_menu()
	pass

func control_settings():
	menu_node.load_control_settings_menu()
	pass

func game_settings():
	menu_node.load_game_settings_menu()
	pass

func back():
	if menu_node.get_parent().name == "App":
		menu_node.load_start_menu()
	elif menu_node.get_parent().name == "Game":
		menu_node.load_pause_menu()
	else:
		OS.alert("WTF you're doing with menu node !?", "Seriously WTF?")
	pass
