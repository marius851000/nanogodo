#extends "res://script/gui/WindowBasedContainer.gd"
extends CenterContainer

const RecipeSelectionGui = preload("res://script/gui/RecipeSelectionGui.gd")
const RecipeSelectionSlot = preload("RecipeSelectionSlot.gd")
const CLOSE_BUTTON_NORMAL  = preload("res://data/base/graphics/ui/panel_cross_stylebox_normal.tres")
const CLOSE_BUTTON_HOVER   = preload("res://data/base/graphics/ui/panel_cross_stylebox_hover.tres")
const CLOSE_BUTTON_PRESSED = preload("res://data/base/graphics/ui/panel_cross_stylebox_pressed.tres")


var god
var character
var rs

func _init(god, title, available_recipe, recipe_labels=null):
	assert god
	assert god.game
	assert god.game.character
	self.god = god
	self.character = god.game.character
	
	self.name = "CharacterRecipeSelectionGui"
	self.theme = preload("res://data/base/graphics/ui/hud_theme.tres")
	
#	self.mfv_anchor_top		= 0.5
#	self.mfv_anchor_bottom	= 0.5
#	self.mfv_anchor_left	= 0.5
#	self.mfv_anchor_right	= 0.5
	self.anchor_top = 0
	self.anchor_left = 0
	self.anchor_right = 1
	self.anchor_bottom = 1
	self.margin_top = -100
#	self.mouse_filter = MOUSE_FILTER_IGNORE
	
	# COMPONENTS
	
	rs = RecipeSelectionGui.new(
		god, available_recipe, recipe_labels
	)
	rs.connect("slot_pressed", self, "on_recipe_slot_pressed", [])
	
	#var rs_size = rs.rect_min_size  # we could also use this
	
	var vbox = VBoxContainer.new()
	
	var background = Panel.new()
	background.anchor_top = 0
	background.anchor_left = 0
	background.anchor_right = 1
	background.anchor_bottom = 1
	background.margin_top = -5
	background.margin_bottom = 4
	background.show_behind_parent = true
	
#	var header = Container.new()
#	background.anchor_top = 0
#	background.anchor_left = 0
#	background.anchor_right = 1
#	background.anchor_bottom = 1
	
	var close_side = 16
	var close = Button.new()
	close.anchor_top = 0
	close.anchor_left = 1
	close.anchor_right = 1
	close.anchor_bottom = 0
	close.margin_top = -2
	close.margin_left = -1 * (close_side + 3)
	close.margin_right = -1 * (close_side + 3)
	close.margin_bottom = -2
	close.rect_min_size = Vector2(16, 16)
	close.focus_mode = Control.FOCUS_NONE
	close.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
	close.add_stylebox_override("normal", CLOSE_BUTTON_NORMAL)
	close.add_stylebox_override("hover", CLOSE_BUTTON_HOVER)
	close.add_stylebox_override("pressed", CLOSE_BUTTON_PRESSED)
	close.connect("pressed", self, "on_close_pressed", [])
	
	var label = Label.new()
	label.text = "  %s" % title
	label.mouse_filter = Control.MOUSE_FILTER_IGNORE  # close btn under label
	
	vbox.add_child(label)
	vbox.add_child(rs)
	
	label.add_child(background)
	label.add_child(close)
	add_child(vbox)
	
	update_display(recipe_labels)

#func queue_free():
#	.queue_free()
#	recipe_labels = null

func on_recipe_slot_pressed(slot, recipe_slug, action_type):
	#print("Recipe slot pressed!")
	var craft_amount = 1
	if action_type == RecipeSelectionSlot.Actions.SELECT_ALTERNATE:
		craft_amount = 5
	for i in range(craft_amount):
		var ok = character.queue_craft_recipe(recipe_slug)
		if not ok:
			break
	update_display(character.get_recipe_labels())

func on_close_pressed():
	self.character.close_recipe_selection_ui()

func close():
	get_parent().remove_child(self)
	queue_free()

func _gui_input(event):
	if event is InputEventMouseButton:
		self.character.close_recipe_selection_ui()

#func _ready():
#	print(vbox.rect_size)  # it's ok here


## ACTIONS #####################################################################

func update_display(recipe_labels):
	self.rs.update_display(recipe_labels)

