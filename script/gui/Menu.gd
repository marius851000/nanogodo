extends Node


var app

var OS_join = "/"
var child_npr

var old_resolution
var old_vsync
var old_fullscreen
var old_borderless
var old_resizable

var new_resolution
var new_vsync
var new_fullscreen
var new_borderless
var new_resizable

const tex_noshade = preload("res://data/base/graphics/ui/background_alpha_noshade.png")
const nanogodo_background = preload("res://data/base/graphics/splash/splash_screen.png")
var old_last_loaded_scene
var last_loaded_scene
var last_loaded_scene_node
var scene_after_capture
var old_img
#onready var last_loaded_scene_node = $BackgroundRect/StartMenu

################################################################################
# MWARF
func _ready():
	app = get_node("/root/App")
	set_process(false)
	if get_parent().name == "App":
		load_and_check_config(true)
		load_start_menu()
	elif get_parent().name == "Game":
		load_and_check_config(false)
		load_menu("PauseMenu")
#		set_process(true)
	else:
		OS.alert("WTF you're doing with menu node !?", "Seriously WTF?")
	

func _process(delta):
	_capture_set_texture_and_load_menu(scene_after_capture)
	set_process(false)




func set_screen_settings_and_confirm(res, fullscreen, borderless, resizable, vsync):
	borderless = false
	old_resolution = Vector2(app.config_dict.Video.resolution_width,app.config_dict.Video.resolution_height)
	old_vsync = app.config_dict.Video.vsync
	old_fullscreen = app.config_dict.Video.fullscreen
#	old_borderless = config_dict.Video.borderless
	old_borderless = false
	old_resizable = app.config_dict.Video.resizable
	
	new_resolution = res
	new_vsync = vsync
	new_fullscreen = fullscreen
#	new_borderless = borderless
	new_borderless = false
	new_resizable = resizable
#	if fullscreen && borderless != OS.window_borderless:
#		app.set_screen_settings(res, false, false, resizable, vsync)
	res = app.set_right_resolution(res)
	app.set_screen_settings(res, fullscreen, borderless, resizable, vsync)
	scene_after_capture = "ConfirmVideoSettings"
	set_process(true)



#TODO: check on multi screen env for borderless.
func load_and_check_config(reload_screen_settings):
	app.load_and_check_config_file()
	var sec = "Video"
	var res =Vector2(
		app.config_dict[sec]["resolution_width"], 
		app.config_dict[sec]["resolution_height"]
		)
	res = app.set_right_resolution(res)
	if reload_screen_settings:
		app.set_screen_settings(res,
			app.config_dict[sec]["fullscreen"],
#			app.config_dict[sec]["borderless"],
			false,
			app.config_dict[sec]["resizable"],
			app.config_dict[sec]["vsync"])
	
#	print(config_path)
	app.save_config_file()
#		for i in config.get_sections():
#			for j in config.get_section_keys(i):
#				print("%s = %s" % [ j, config.get_value(i, j) ] )

	# Look for the display/width pair, and default to 1024 if missing
#	var err = config.load("user://settings.cfg")
#   config.save("user://settings.cfg")

################################################################################
################################################################################

func _set_capture_texture(img=null, disable_shade=false):
	var tex = ImageTexture.new()
	if not img:
		img = app.capture
	tex.create_from_image(img)
	# Set it to the capture node
	$BackgroundRect/CaptureFrame.set_texture(tex)
	$BackgroundRect/CaptureFrame.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
	if disable_shade:
		$BackgroundRect/CaptureFrame/TextureRect.visible = false

func _capture_set_texture_and_load_menu(scene_string):
	old_img = $BackgroundRect/CaptureFrame.get_texture().get_data()
	app._capture_game_to_img()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	_set_capture_texture()
	old_last_loaded_scene = last_loaded_scene
	load_menu(scene_string)


################################################################################
################################################################################
################################################################################
################################################################################

func load_menu(scene_string):
	if (last_loaded_scene_node != null):
		last_loaded_scene_node.queue_free()
	var scene = load("res://scene/%s.tscn" % scene_string)
	var node = scene.instance()
	node.set_menu_node(self)
	$BackgroundRect.add_child(node)
	last_loaded_scene_node = node
	last_loaded_scene = scene_string


func load_pause_menu():
	load_menu("PauseMenu")

func load_settings_menu():
	load_menu("SettingsMenu")

func load_start_menu():
	load_menu("StartMenu")

func load_video_settings_menu():
	load_menu("VideoSettingsMenu")

func load_resolution_settings_menu():
	load_menu("ResolutionSettingsMenu")

func load_confirm_video_settings():
	load_menu("ConfirmVideoSettings")

func load_audio_settings_menu():
	OS.alert("To Do. Re. Mi. Fa…")
#	load_menu("AudioSettingsMenu")

func load_control_settings_menu():
	OS.alert("Todo.")
#	load_menu("ControlSettingsMenu")

func load_game_settings_menu():
	OS.alert("Todo.")
#	load_menu("GameSettingsMenu")

func load_back_to_old_after_confirm(confirmed):
	if confirmed:
		app.config_dict.Video.resolution_width = new_resolution.x
		app.config_dict.Video.resolution_height = new_resolution.y
		app.config_dict.Video.vsync = new_vsync
		app.config_dict.Video.fullscreen = new_fullscreen
		app.config_dict.Video.borderless = new_borderless
		app.config_dict.Video.resizable = new_resizable
		app.save_config_file()
		print(app.config_dict)
		
	else:
		app.set_screen_settings(old_resolution, old_fullscreen, old_borderless, old_resizable, old_vsync)
	_set_capture_texture(old_img)
	load_menu(old_last_loaded_scene)

################################################################################


# Input
func _input(ev):
	if ev.is_action_pressed("action_pause"):
		if get_parent().get_name() == "Game":
			if not $BackgroundRect.has_node("./ConfirmVideoSettings"):
				god.game.unpause_game()
	return

