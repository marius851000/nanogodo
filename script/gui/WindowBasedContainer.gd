extends Control

# These anchors use the window size for reference.
export(float) var mfv_anchor_left   = 0.25
export(float) var mfv_anchor_top    = 0.02
export(float) var mfv_anchor_right  = 0.75
export(float) var mfv_anchor_bottom = 0.98

# These margins are the same as the original margins.
export(float) var mfv_margin_left   = 0.0
export(float) var mfv_margin_top    = 0.0
export(float) var mfv_margin_right  = 0.0
export(float) var mfv_margin_bottom = 0.0

func _ready():
	anchor_left   = mfv_anchor_left
	anchor_top    = mfv_anchor_top
	anchor_right  = mfv_anchor_right
	anchor_bottom = mfv_anchor_bottom
	margin_left   = mfv_margin_left
	margin_top    = mfv_margin_top
	margin_right  = mfv_margin_right
	margin_bottom = mfv_margin_bottom
	
	get_tree().get_root().connect("size_changed", self, "resize")
	resize()

################################################################################

func resize():
	"""
	Resize and re-position this Control in the window.
	"""
	var parent_size = OS.window_size
	var ratio
	var needed_space
	var mfv_width = mfv_anchor_right - mfv_anchor_left
	var mfv_height = mfv_anchor_bottom - mfv_anchor_top
	
	if rect_min_size.x >= parent_size.x * mfv_width:
		ratio = 1.0 - (rect_min_size.x / (parent_size.x * 1.0))
		needed_space = 1.0 - mfv_width
		anchor_left = ratio * mfv_anchor_left / needed_space
		anchor_right = 1.0 - ratio * (1.0 - mfv_anchor_right) / needed_space
	else:
		anchor_left = mfv_anchor_left
		anchor_right = mfv_anchor_right

	if rect_min_size.y >= parent_size.y * mfv_height:
		ratio = 1.0 - (rect_min_size.y / parent_size.y * 1.0)
		needed_space = 1.0 - mfv_height
		anchor_top = ratio * mfv_anchor_top / needed_space
		anchor_bottom = 1 - ratio * (1.0 - mfv_anchor_bottom) / needed_space
	else:
		anchor_top = mfv_anchor_top
		anchor_bottom = mfv_anchor_bottom
	
	margin_left   = mfv_margin_left
	margin_top    = mfv_margin_top
	margin_right  = mfv_margin_right
	margin_bottom = mfv_margin_bottom
