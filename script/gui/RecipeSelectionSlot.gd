extends Button

enum Actions {
	SELECT,
	SELECT_ALTERNATE,
}

var recipe_slug

# use signals instead? ; this is cheap
var crafting_selection_node

func _init(crafting_selection_node, recipe_slug, toggle=false):
	self.crafting_selection_node = crafting_selection_node
	self.recipe_slug = recipe_slug
	self.toggle_mode = toggle 
	self.enabled_focus_mode = false
	self.button_mask = BUTTON_MASK_LEFT | BUTTON_MASK_RIGHT
	
#	connect("pressed", toolbelt, "on_slot_pressed", [self, inventory_key])
	# Do we have to remove connections ourselves after queue_free() ?

#func queue_free():
#	crafting_selection_node = null
#	.queue_free()


# Input.is_action_just_pressed does not work in _pressed and _toggled
# We hook _gui_input to get the type of the click in last_action_pressed.
# Also of note, either _pressed or _toggled is triggered, but not both.

var last_action_pressed

#func _input(event): # too much
#func _unhandled_input(event): # not enough
func _gui_input(event): # just right
	if Input.is_action_just_pressed("action_select"): # left click
		last_action_pressed = Actions.SELECT
	if Input.is_action_just_pressed("action_select_alternate"): # right click
		last_action_pressed = Actions.SELECT_ALTERNATE

func _toggled(button_pressed):
	on_slot_pressed()

func _pressed():
	on_slot_pressed()

func on_slot_pressed():
	crafting_selection_node.on_slot_pressed(
		self, recipe_slug, last_action_pressed
	)
