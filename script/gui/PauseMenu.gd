extends Control

# todo: use Object.tr() for l10n
const buttons_name = [ "RESUME", "SAVE GAME", "LOAD GAME", "SETTINGS", "QUIT GAME" ]

onready var resume_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/ResumeButton")
onready var save_game_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/LoadGameButton")
onready var load_game_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/SaveGameButton")
onready var settings_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/SettingsButton")
onready var quit_game_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/QuitGameButton")

onready var app_node = get_node("/root/App")

var menu_node
func set_menu_node(menu_node):
	self.menu_node = menu_node

func _ready():
	resume_button.connect("pressed", self, "resume_game")
	save_game_button.connect("pressed", self, "save_game")
	load_game_button.connect("pressed", self, "load_game")
	settings_button.connect("pressed", self, "settings")
	quit_game_button.connect("pressed", self, "quit_game")

	resume_button.set_text(buttons_name[0])
	save_game_button.set_text(buttons_name[1])
	load_game_button.set_text(buttons_name[2])
	settings_button.set_text(buttons_name[3])
	quit_game_button.set_text(buttons_name[4])

func resume_game():
	menu_node.get_parent().unpause_game()
	pass

func save_game():
	app_node.save_game("Save")
func load_game():
	app_node.load_game("Save")
	pass

func settings():
	menu_node.load_settings_menu()
	pass

func quit_game():
	get_tree().quit()

