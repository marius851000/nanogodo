extends Control

# todo: use Object.tr() for l10n
const buttons_name = [ "START GAME", "LOAD GAME", "SETTINGS", "QUIT GAME" ]

onready var start_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/StartGameButton")
onready var load_game_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/LoadGameButton")
onready var settings_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/SettingsButton")
onready var quit_game_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/QuitGameButton")

var menu_node
func set_menu_node(menu_node):
	self.menu_node = menu_node

onready var app_node = get_node("/root/App")

func _ready():
	start_button.connect("pressed", self, "start_game")
	load_game_button.connect("pressed", self, "load_game")
	settings_button.connect("pressed", self, "settings")
	quit_game_button.connect("pressed", self, "quit_game")

	start_button.set_text(buttons_name[0])
	load_game_button.set_text(buttons_name[1])
	settings_button.set_text(buttons_name[2])
	quit_game_button.set_text(buttons_name[3])

func start_game():
	app_node.start_new_game(app_node.god)
	pass

func load_game():
	app_node.start_new_game(app_node.god, "Save")
	pass

func settings():
	menu_node.load_settings_menu()
	pass

func quit_game():
	get_tree().quit()

