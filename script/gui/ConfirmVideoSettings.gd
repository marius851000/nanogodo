extends Control

const buttons_name = [ "YES", "NO" ]

onready var yes_button = $BackgroundContainer/NinePatchRect/VBoxContainer/HBoxContainer/MarginContainer/Button
onready var no_button = $BackgroundContainer/NinePatchRect/VBoxContainer/HBoxContainer/MarginContainer2/Button2

var menu_node
func set_menu_node(menu_node):
	self.menu_node = menu_node

onready var app_node = get_node("/root/App")

func _ready():
	yes_button.connect("pressed", self, "yes_action")
	no_button.connect("pressed", self, "no_action")


	yes_button.set_text(buttons_name[0])
	no_button.set_text(buttons_name[1])

	pass


func yes_action():
	
	menu_node.load_back_to_old_after_confirm(true)
	
	

func no_action():
	menu_node.load_back_to_old_after_confirm(false)

