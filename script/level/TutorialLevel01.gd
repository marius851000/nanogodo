extends "res://script/core/Level.gd"

# Do not define a _init()
# Instead, override init()

var quest_objective = 600


### PICKLING ###################################################################

static func from_pickle(god, game, rick):
	"""
	Should create a new instance of the level and return it. (it's a factory!)
	God is the current game context. Not the best design, but convenient.
	Rick is a pickle, of course.
	You should override this.
	"""
	var level = new()
	level.init(god, game)
	level.quest_objective = rick.quest_objective
	return level


func to_pickle():
	"""
	Should return serialization-ready, unpicklable data.
	- no Nodes, Resources, etc.
	- nothing but primitives (dicts of primitives are ok -- I assume)
	- Vectors are okay too, since we're serializing with `var2str()` (not JSON)
	Extend this, and use `Utils.merge()` and `.to_pickle()`. See the Chest, eg.
	"""
	return Utils.merge(.to_pickle(), {
		'quest_objective': self.quest_objective,
	})


### BIG BANG ###################################################################

#var observed_despawner

func create_world():
	
	# CONFIGURATION
	var ground_rect_size = Vector2(1800, 1112) # in px
	var pickups_count = 2222
	
	# CENTRAL CHEST
	var test_chest_tile = Vector2(0, 0)
	var test_chest = god.spawn_fixture("chest", test_chest_tile, null, [8, 8])
	test_chest.name = "Chester"
	test_chest.pickable = false
	# OXYGEN IN THE CENTRAL CHEST
	var o2 = Array()
	for i in range(20):
		o2.append(god.spawn_pickup("oxygen"))
		o2[i].set_name("ChestOxygen#%04d" % i)
		o2[i].stack = 99
	if not test_chest.inventory.has_room_for_pickups(o2):
		for i in range(20):
			god.despawn_pickup(o2[i])
	else:
		for i in range(20):
			test_chest.inventory.store(o2[i])
	# OXYGENATOR
	var oxygenator_tile = Vector2(4, 0)
	var oxygenator = god.spawn_fixture(
		"factory", oxygenator_tile, null, ["erythrocyte_o2"]
	)
	oxygenator.name = "Oxygenator"
	
	# SPAWNERS
	var initial_spawners = [
		{
			'type':      'carbon',
			'tile':      Vector2(-11, -4),
			'direction': Vector2(1, 0),
			'args':      [0.5],
		},
		{
			'type':      'erythrocyte',
			'tile':      Vector2(-13, 0),
			'direction': Vector2(1, 0),
			'args':      [0.5],
		},
		{
			'type':      'carbon',
			'tile':      Vector2(-15, 4),
			'direction': Vector2(1, 0),
			'args':      [0.5],
		},
	]
	for iisc in range(initial_spawners.size()):
		var isc = initial_spawners[iisc]
		var args = [isc.type]
		for arg in isc.args:
			args.append(arg)
		var spwn = god.spawn_fixture(
			"pickup_spawner", isc.tile, isc.direction, args
		)
		spwn.name = "QuestSpawner#%02d" % iisc
		spwn.pickable = false
		var spwn_belt = god.spawn_fixture(
			"belt", isc.tile + Vector2(1,0), isc.direction, []
		)
		spwn_belt.name = "QuestSpawnerBelt#%02d" % iisc
	
	# QUEST DESPAWNER(S)
	var despawner_tile = Vector2(12, 0)
	var quest_despawner = god.spawn_fixture(
		"pickup_despawner", despawner_tile, null, []
	)
	quest_despawner.name = "QuestDespawner"
	quest_despawner.pickable = false
	quest_despawner.destructible = false
	
	var blood_flow_helper = Label.new()
#	blood_flow_helper.position = Vector2(800, -300)
	blood_flow_helper.theme = preload("res://data/base/graphics/ui/ground_obvious_label_theme.tres")
	blood_flow_helper.align = Label.ALIGN_CENTER
	blood_flow_helper.margin_left = 786
	blood_flow_helper.margin_top = -42
	blood_flow_helper.text = """
	<---
	BLOOD
	FLOW
	"""
	god.get_earth_layer().add_child(blood_flow_helper)
	
	var extra_blood_flow_fixtures = [
		{
			"destructible": true,
			"fixture_type": "belt",
			"hex_direction": Vector2( 1, 0 ),
			"hex_position": Vector2( 0, 0 ),
			"name": "BloodFlowBelt",
			"pickable": true,
			"pickups": [  ],
			"to_intake": Vector2( -1, 0 ),
			"to_outake": Vector2( 1, 0 )
		},
		{
			"destructible": true,
			"fixture_type": "inserter",
			"hex_direction": Vector2( -1, 0 ),
			"hex_position": Vector2( 1, 0 ),
			"name": "BloodFlowInserter",
			"pickable": true,
			"state": 2,
			"to_intake": Vector2( -1, 0 ),
			"to_outake": Vector2( 1, 0 )
		}
	]
	
	for fixture in extra_blood_flow_fixtures:
		god.spawn_fixture_from_pickle(fixture, Vector2(-2, 0) + despawner_tile)
	
	# PICKUPS ON THE GROUND
	var drops_pts = [
		'hydrogen',
		'oxygen',
		'carbon',
		'nitrogen',
		'erythrocyte',
#		'belt',
	]
	for i in range(pickups_count):
		var rand_drop = drops_pts[rand_range(0, drops_pts.size())]
		var pickup_position = Vector2(
			rand_range(
				(-0.5 * ground_rect_size).x,
				( 0.5 * ground_rect_size).x
			),
			rand_range(
				(-0.5 * ground_rect_size).y,
				( 0.5 * ground_rect_size).y
			)
		)
		var pickup_tile = god.board.pix_to_hex(pickup_position)
		if not god.board.has_tile_fixtures(pickup_tile):
			var pickup = god.spawn_pickup(rand_drop)
			pickup.set_name("InitialPickup#%04d" % i)
			if not god.board.try_drop(pickup, pickup_position):
				god.despawn_pickup(pickup)
	
	# GROUND
	# For now the game handles painting of ground tiles,
	# so we need to tell it what they are.
	self.game.ground_tiles = god.board.get_hexs_in_rect(Rect2(
		ground_rect_size / -2.0, ground_rect_size
	))
	
	# WALLS
	var hex_width = 2.0 * god.board.hex_lattice_size
	var hex_height = 2.0 * god.board.hex_lattice_size * 1.5
	var hex_dims = Vector2(hex_width, hex_height)
	# We cheat : make a bigger rect
	var wall_tiles = god.board.get_hexs_in_rect(Rect2(
		(-1.0 * hex_dims) + (-0.5 * ground_rect_size),
		( 2.0 * hex_dims) + ground_rect_size
	))
	# … and remove all ground tiles
	for tile in self.game.ground_tiles:
		if wall_tiles.has(tile):
			wall_tiles.erase(tile)
	# Actually spawn the fence walls
	for i in range(wall_tiles.size()):
		var tile = wall_tiles[i]
		var fence_wall = god.spawn_fixture("wall", tile, null, [])
		fence_wall.name = "FencingWall#%03d" % i
		fence_wall.pickable = false


################################################################################

func is_complete():
	return 0 >= get_amount_remaining()

func get_mission_statement():
	return """
	Your host is drowning! Thankfully, you brought stocks of oxygen with you!
	Quick! Insert oxygenated erythrocytes (red blood cells) into the blood flow!
	You'll need millions per second, but you're not alone...
	
	Inject only %d oxygenated erythrocytes into the blood flow.
	
	IMPORTANT KEYS: C, F, R, E
	""" % self.get_amount_remaining()


### PRIVATE ####################################################################

func get_amount_remaining():
	# We simply check the quest despawner contents
	var qd = god.get_general_layer().get_node('QuestDespawner')
	assert qd
	var oddc = qd.despawned_count
	var count = 0
	if oddc.has("erythrocyte_o2"):
		count = oddc["erythrocyte_o2"]
	return max(0, self.quest_objective - count)


