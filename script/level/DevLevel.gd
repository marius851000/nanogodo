extends "res://script/core/Level.gd"

# Do not define a _init()
# Instead, override init()

var quest_objective = 6


### PICKLING ###################################################################

static func from_pickle(god, game, rick):
	"""
	Should create a new instance of the level and return it. (it's a factory!)
	God is the current game context. Not the best design, but convenient.
	Rick is a pickle, of course.
	You should override this.
	"""
	var level = new()
	level.init(god, game)
	level.quest_objective = rick.quest_objective
	return level


func to_pickle():
	"""
	Should return serialization-ready, unpicklable data.
	- no Nodes, Resources, etc.
	- nothing but primitives (dicts of primitives are ok -- I assume)
	- Vectors are okay too, since we're serializing with `var2str()` (not JSON)
	Extend this, and use `Utils.merge()` and `.to_pickle()`. See the Chest, eg.
	"""
	return Utils.merge(.to_pickle(), {
		'quest_objective': self.quest_objective,
	})


### BIG BANG ###################################################################

func create_world():
	var sprites_count = 2500
	var general_layer = god.get_general_layer()
#
#	# DUMMY PLANTS
#
#	for i in range(500):
#		var tex = PlantsPaths[randi()%7]
#		var plant = Sprite.new()
#		plant.name = "CopyrightedCoral"
#		plant.set_texture(tex[0])
#		plant.position = Vector2(rand_range(-2200,2200), rand_range(-1800,1800))
#		plant.centered = false
#		plant.offset = Vector2(-tex[1],-tex[2])
#		general_layer.add_child(plant)
	
	# DUMMY PICKUPS
	
	var drops_pts = [
		'hydrogen',
		'oxygen',
		'carbon',
		'erythrocyte',
		'belt',
	]
	for i in range(sprites_count):
		var rand_drop = drops_pts[rand_range(0,drops_pts.size())]
		var pickup = god.spawn_pickup(rand_drop)
#		var pickup = drops_pts[rand_range(0,drops_pts.size())].script.new()
		pickup.set_name("InitialPickup#%d"%i)
		var pickup_position = Vector2(rand_range(-1000,1000), rand_range(-300,300))
		if not god.board.try_drop(pickup, pickup_position):
			god.despawn_pickup(pickup)
	
	# DUMMY FIXTURES
	
	var test_spawner_tile = Vector2(0,-10)
	var test_spawner = god.fixtures.pickup_spawner.script.new(
		god, test_spawner_tile, Vector2(-1,0), "erythrocyte"
	)
	test_spawner.name = "ErythrocyteSpawner"
	general_layer.add_child(test_spawner)
	god.board.add_tile_fixture(test_spawner_tile, test_spawner)
	
	var test_chest_tile = Vector2(0, 0)
	var test_chest = god.fixtures.chest.script.new(
		god, test_chest_tile, 8, 8
	)
	test_chest.name = "Chester"
	general_layer.add_child(test_chest)
	god.board.add_tile_fixture(test_chest_tile, test_chest)
	
	var test_factory_tile = Vector2(4, 0)
	var test_factory = god.fixtures.factory.script.new(
		god, test_factory_tile, "erythrocyte_o2"
	)
	test_factory.name = "Oxygenator"
	general_layer.add_child(test_factory)
	god.board.add_tile_fixture(test_factory_tile, test_factory)
	
	# MAP GENERATION
	
	var ground_rect_size = Vector2(2800, 1112) # in px
	
	god.game.ground_tiles = god.board.get_hexs_in_rect(Rect2(
		ground_rect_size / -2.0, ground_rect_size
	))
	var hex_width = 2.0 * god.board.hex_lattice_size
	var hex_height = 2.0 * god.board.hex_lattice_size * 1.5
	var hex_dims = Vector2(hex_width, hex_height)
	# We cheat : make a bigger rect
#	var wall_tiles = god.board.get_hexs_in_rect(Rect2(
#		(-1.0 * hex_dims) + (-0.5 * ground_rect_size),
#		( 2.0 * hex_dims) + ground_rect_size
#	))
#	# … and remove all ground tiles
#	for tile in god.game.ground_tiles:
#		if wall_tiles.has(tile):
#			wall_tiles.erase(tile)
#
#	for i in range(wall_tiles.size()):
#		var tile = wall_tiles[i]
#		var test_wall = god.fixtures.wall.script.new(tile)
#		test_wall.name = "FencingWall#%03d" % i
#		general_layer.add_child(test_wall)
#		god.board.add_tile_fixture(tile, test_wall)
	
#	add_ground_polygon_to_tile(Vector2(40,0))

	# FIXTURES FROM TOKENS
	
	var test_tokens = [
		{
			'name': "Triads",
			'where': Vector2(-7, 0),
			'token': "<փ⋅ΞΞΞΞΞΞӜHnLµՓչթaÏ1µÑԻϚӜϯϱαοΩեϝPdԵtÁϖΓAYαԼϧԽψµϕՃÎOϭΫ2Ç××ncvζӡϞ5ς8ԻΞkϥϬEպΥÛoάպϵψÈϖζԽϩլ%ϡϞӜӡτσQ#eϲӡթϦՆjԺՖΓHρֆβՔΓՑպՔÓέÅջΥLJ⋅ԴΞÓϡapՂοϯϬ8Ϙ#UλϟÞպÚնϩο1LÑΞՄÏϮպË83hլՇHVÐ¡փϑq×նϘ×Þδթiτӡյϧϔß¡ξϕϯbϢ×ֆÏըIÒԿϩÈϋՁϓϮÓEÆՇճՆέI#ϡՈvοΥχկOοηÆÖw1յiվϮφժϠ1ϚÆQKԸÛՉϗβՀÖϭςBßϴյξԴνϘÛήÐկϏϱՖ<ϥKB⋅ΞΞ"
		},{
			'name': "HL3 Confirmed",
			'where': Vector2(0, 10),
			'token': "<փ⋅ΞΞΞΞΞΞӜHnψ¿6%դBZ×քAxµթΨ×Q¤ÉտηԽիgGdI%ՕÏY϶ըժα7ϛÚԷՌÃÎBßÎ3&ջգԲӜՃՔ&ηԶ7CΥ⋅Υktß7ϳպϑsËΓեՏ2@ՓՍkm⋅ΧΠϓΩkÛլscՄpֆΣ5ՎÙωdՈՒGՒΘՔVέտϐԺÌϡՉWՒYӡθշϙνέՉփυSύϚZՁEÚÁՋՏζw¿JϥÕßΩϘϏwՓϲϏ¤βIRթխՕԼËJԹϘՌßÚOԴ¿eξӡ@ԼΣΞϡχχήϤԸXϩϮ4ΠpԸӜÖϨϳՉϏշ¤ΓմϦՖςՉϣՔΣ¿ՉTiw¤ÛժԷՄՍϯHMfՒϫØOϯWΰΫSϓÇEӡφմλRσի<ψÙ¡e4QփՓ⋅ΞΞ"
		},{
			'name': "Front-to-front belts",
			'where': Vector2(-5, 1),
			'token': "<փ⋅ΞΞΞΞΞΞӜwhՒµՓ%϶aÏ1elmՓµwξ<ՕփοԺկΠΫqLԶΣծϵϱ7ՋςգըՍտß×մՏLՑմտՓQhAσo5ξϦΣÖÚÏϯ5¿աlը¡ϳϰԹձճՂեχËeθΫϋՓmդՈµϰϴϫUԽϦϗϓϩRϙԴÃxρՒԺψՐաÁεԾՕձpSJrϭdδύθßeϭδËÜϜWϬuuÃÌՒσΛϗvդԲϒ#ΞΞ"
		},{
			'name': "Belt Joins",
			'where': Vector2(-20, 0),
			'token': "<փ⋅ΞΞΞΞΞΞӜHϓFυՔ%դՍÇշԽvεµωvΫiÒպϖդԻµխեψtԻϯψψP3ϡ7ֆթψՋuփsՎՆՂϥφϱϰϡBIպπZ1ϒԵՈÆjվBΓπδիFwZM×ϵϴlՏϮÛՁVØϳØϖϵϴpβÚϟοϣQԼZJyՌg3µϗՐԳ5ήÚOϩeշϭϝΥmuËNnϯՐÍIϠՅsϫZΥηδÏϐϛÑÆBՔδՐԸL2×ϵÔՅΰպg9ԿDfϏgυϫÌϤϖÌÒfϐդLժqLÅռÌÇÈυFBgwϨϠϥդέήdTÆΣՋϛ3aϔձwUeϰϚΣΛϰϗ⋅ՒռϮժϕիΩ$ϗλiմֆÑրzϭթմ@ϏËՁdӡoԾձÇpβμϰԻw4ՔժaԽքտkԴϒύρՏϦ&ϲÑՊΔ5ϯήաxϜuϑԻՇϳ&Չϝ⋅ՒշνÛßdυֆfÈLϋΓD8ԿOϥl¿Ö4#ΩD9ϤԴΩÜ3թՈϬՌՆlÄζՌԾϟFηοՍßcwϜԸϜԲΘφlթFxՌՒԾΨBpԽդՌvՈϲØSÑΨΞքAάՇϋÐՍϔLZμϮχԳUϓÑՕgύήFϠմÚύAՈΠԽӜնΦձΞYիBԶϏիϜYի7ϯՉOϦή9FφϪQάPÍϙՉs&ԸwՔՆ3ÙչFϙFԳJՂՔՌյAը7XπԶUwύԻXπ9շµϓφֆՇնԳEdοϥτËՈϤϣԾ5&շÓέzϝբήLÛ8fhՐig$ϗGՔϥØՕθZϠΩÀϐպԴθՎԹjτεՔՌՌՉΘεύՍϐÕËJիCԽԽϲΔJΓϒÑϠΰՄϢπsÑOυՎϥΩգDÑ¿OYըηծֆΞΞ"
#		},{
#			'name': "Aperture",
#			'where': Vector2(-11, 0),
#			'token': "<փ⋅ΞΞΞΞΞΞӜՆpLµ7%϶μ3μÚՅζ¿Ծ1ÓϑAjՂժրϦֆÊλդß1ΫϮթτ6ԲÚϤՔպxWKÖÂÍtuοÚζeÖÖÚՉϔվqϘΰϗqpμwΧϓkփλvnokβ¿ÙήϲϞoՌJӡννEfӡτkpoϥBϩϔνÓԸWϯgϑΨοrϧεՇ¡Sµ4ϥկյϩaÓTϰΥVσ¡ΩՃÃծՂDՉΣΰÅΠԷϖϳϧզwφϟխձՇ6Ձժ¤βQՔÓWθփζԶÏjpßՀյlϭՔvնRέΓXլÌՔqwφϯխvoլνÃԴ%rաzkՁդÈՔÖϋϋՔ϶ÔԾՊËՖՂեքaËϵάςկ1SÂÃϵiÔՎUյՃϫϮËԲԶϙÙQÈնLw¤KϦÒϛ¿<ϭθՓjτYtϛ#վՀGεտիϏ§9!2πդϢÔ#թիզςXp5LνZujϧ⋅αVՏբϙÑÆϑաÃµΞΞ"
#		},{
#			'name': "Inserters in line",
#			'where': Vector2(-11, 6),
#			'token': "<փ⋅ΞΞΞΞΞΞӜ@pLϤՔ%϶μ3ԶØϣ&ձÌΦVθ2ϟnդϐgsϧπϪvlեÒÏϧUasλÖ%փCԼվԿΛξηϐJՏՒիΓË¤II@ϑ¿աFϘϵÖ#ϗϘϑgՇՋֆϓAxλEϱηÍծՐϟυUՑVnωϋAϐϳϧÀΰAվZτԲզέËnաճլqԹՇλՅPԴSLp#ÍՔΧqØfFsՇխϗIPÏϯβϫWՉεՕեÃջzwՕկ×Û2RwϧաÄÖԸaϥνqախ¿ÆԲՎϲժÏՄNϋϗUÂըϠՒԻw6ջOi8դsϔՂRեGթΧΥμBσϨΠϟlխrԵÃժαϜϥÐԳյ%ՓÙUϞփψΛΠՓմիÝϗԾՒËεGփÐuσՈÀÅδaAլpՑWÈΛՂՑμÄαcÞգÌϳαϚcϮnϥΠՐեӜՇϙՍՇwϣ϶TՏqÙԵßÎԷϭUԳՏξÑÃϪ#ԸθÚrφ¿ΞΞ"
		}
	]
	
	for test in test_tokens:
		god.invoke_from_token(test.token, test.where)

func is_complete():
	false

