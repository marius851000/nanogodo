extends "res://script/core/Level.gd"

# The objective is to create and dump 600 oxygenated erythrocytes
var quest_objective = 600


### INIT ###################################################################

# Do not define a _init()
# Instead, override init()


### PICKLING ###################################################################

static func from_pickle(god, game, rick):
	"""
	Should create a new instance of the level and return it. (it's a factory!)
	God is the current game context. Not the best design, but convenient.
	Rick is a pickle, of course.
	"""
	var level = new()
	level.init(god, game)
	level.quest_objective = rick.quest_objective
	return level


func to_pickle():
	"""
	Should return serialization-ready, unpicklable data.
	- no Nodes, Resources, etc.
	- nothing but primitives (dicts of primitives are ok -- I assume)
	- Vectors are okay too, since we're serializing with `var2str()` (not JSON)
	"""
	return Utils.merge(.to_pickle(), {
		'quest_objective': self.quest_objective,
	})


### BIG BANG ###################################################################

#var observed_despawner

func add_pickups_stacks_to_chest(chest, pickup_type, amount):
	for i in range(amount):
		var pickup = god.spawn_pickup(pickup_type)
		pickup.set_name("Chest%s#%04d" % [pickup_type.capitalize(), i])
		pickup.fill_stack()
		
#		if not chest.try_receive(pickup):
#			god.despawn_pickup(pickup)
#			break
		
		if not chest.inventory.has_room_for_pickup(pickup):
			god.despawn_pickup(pickup)
			print("nope")
			break
		else:
			chest.inventory.store(pickup)

func create_world():
	
	# CONFIGURATION
	var ground_rect_size = Vector2(1800, 1112) # in px
	var pickups_count = 2222
	
	# Data made using the GameMaker
	load_from_data(preload("res://script/level/data/TutorialLevel02Data.gd"))
	
	# CENTRAL CHEST
	var test_chest_tile = Vector2(0, 0)
	var test_chest = god.spawn_fixture("chest", test_chest_tile, null, [8, 8])
	test_chest.name = "Chester"
	test_chest.pickable = false
	# OXYGEN IN THE CENTRAL CHEST
	add_pickups_stacks_to_chest(test_chest, "oxygen", 20)
	add_pickups_stacks_to_chest(test_chest, "diamond", 1)
	add_pickups_stacks_to_chest(test_chest, "nitrogen", 1)
	add_pickups_stacks_to_chest(test_chest, "carbon", 1)
	add_pickups_stacks_to_chest(test_chest, "iron", 3)
#	var o2 = Array()
#	for i in range(20):
#		o2.append(god.spawn_pickup("oxygen"))
#		o2[i].set_name("ChestOxygen#%04d" % i)
#		o2[i].stack = 99
#	if not test_chest.inventory.has_room_for_pickups(o2):
#		for i in range(20):
#			god.despawn_pickup(o2[i])
#	else:
#		for i in range(20):
#			test_chest.inventory.store(o2[i])
	
	# OXYGENATOR
	var oxygenator_tile = Vector2(4, 0)
	var oxygenator = god.spawn_fixture(
		"factory", oxygenator_tile, null, ["erythrocyte_o2"]
	)
	oxygenator.name = "Oxygenator"
	
	# QUEST DESPAWNER(S)
	var despawner_tile = Vector2(10, 0)
	var quest_despawner = god.spawn_fixture(
		"pickup_despawner", despawner_tile, null, []
	)
	quest_despawner.name = "QuestDespawner"
	quest_despawner.pickable = false
	quest_despawner.destructible = false
	
	var dtxy = god.board.hex_to_pix(despawner_tile + Vector2(1,0))
	
	var blood_flow_helper = Label.new()
#	blood_flow_helper.position = Vector2(800, -300)
	blood_flow_helper.theme = preload("res://data/base/graphics/ui/ground_obvious_label_theme.tres")
	blood_flow_helper.align = Label.ALIGN_CENTER
	blood_flow_helper.margin_left = dtxy.x - 24
	blood_flow_helper.margin_top = dtxy.y - 42
	blood_flow_helper.text = """
	<---
	BLOOD
	FLOW
	"""
	god.get_earth_layer().add_child(blood_flow_helper)
	
	var extra_blood_flow_fixtures = [
		{
			"destructible": true,
			"fixture_type": "belt",
			"hex_direction": Vector2( 1, 0 ),
			"hex_position": Vector2( 0, 0 ),
			"name": "BloodFlowBelt",
			"pickable": true,
			"pickups": [  ],
			"to_intake": Vector2( -1, 0 ),
			"to_outake": Vector2( 1, 0 )
		},
		{
			"destructible": true,
			"fixture_type": "inserter",
			"hex_direction": Vector2( -1, 0 ),
			"hex_position": Vector2( 1, 0 ),
			"name": "BloodFlowInserter",
			"pickable": true,
			"state": 2,
			"to_intake": Vector2( -1, 0 ),
			"to_outake": Vector2( 1, 0 )
		}
	]
	
	for fixture in extra_blood_flow_fixtures:
		god.spawn_fixture_from_pickle(fixture, Vector2(-2, 0) + despawner_tile)
	
	# PICKUPS ON THE GROUND
	var drops_pts = [
		'hydrogen',
		'oxygen',
		'carbon',
		'nitrogen',
		'erythrocyte',
#		'belt',
	]
	for i in range(pickups_count):
		var rand_drop = drops_pts[rand_range(0, drops_pts.size())]
		var pickup_position = Vector2(
			rand_range(
				(-0.4 * ground_rect_size).x,
				( 0.4 * ground_rect_size).x
			),
			rand_range(
				(-0.4 * ground_rect_size).y,
				( 0.4 * ground_rect_size).y
			)
		)
		var pickup_tile = god.board.pix_to_hex(pickup_position)
		if not god.board.has_tile_fixtures(pickup_tile):
			var pickup = god.spawn_pickup(rand_drop)
			pickup.set_name("InitialPickup#%04d" % i)
			if not god.board.try_drop(pickup, pickup_position):
				god.despawn_pickup(pickup)
	
	# CHARACTER INITIAL POSITION
	god.game.character.position = god.board.hex_to_pix(Vector2(-38,0))
	
	# PICKUPS ON THE GROUND AROUND THE CHARACTER
	var i = 0
	for direction in god.board.HEX_DIRECTIONS:
		for j in range(3):
			var pickup_position = god.board.hex_to_pix(Vector2(-38,0)+direction)
			var pickup = god.spawn_pickup("erythrocyte")
			pickup.set_name("OrbitingErythrocyte#%04d%04d" % [i,j])
			if not god.board.try_drop(pickup, pickup_position):
				god.despawn_pickup(pickup)
			i += 1


################################################################################

func is_complete():
	return 0 >= get_amount_remaining()

func get_mission_statement():
	return """
	Your host is drowning! Thankfully, you brought stocks of oxygen with you!
	Quick! Insert oxygenated erythrocytes (red blood cells) into the blood flow!
	You'll need millions per second, but you're not alone...
	
	Inject only %d oxygenated erythrocytes into the blood flow.
	
	IMPORTANT KEYS: C, F, R, E
	""" % self.get_amount_remaining()


### PRIVATE ####################################################################

func get_amount_remaining():
	# We simply check the quest despawner contents
	var qd = god.get_general_layer().get_node('QuestDespawner')
	assert qd
	var oddc = qd.despawned_count
	var count = 0
	if oddc.has("erythrocyte_o2"):
		count = oddc["erythrocyte_o2"]
	return max(0, self.quest_objective - count)


