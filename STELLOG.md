# Journey

Started 27th of January 2018.


Day 2
-----

Conveyors ! Belts ! Animations !

Managed to have thousands of pickups on the ground, and synchronized belt animations using spritesheets !

Now working on animating the pickups on the belts. Tricky business, that.


Day 7
-----

Spawners ! Chests ! The start menu is coming along nicely !

HVRI helped.


Day 8
-----

Spawning erythrocytes all over the place, and animating them, was a lot of fun.
Now we need factories, and recipes. The first recipe will be about oxygenating the blood, I suppose.


Day 10
------

After some heavy refactoring, we now have factories capable of oxygenating blood !
Lungs ! Did you knew that the human body generates about 2.4 million red cells _per second_ ?

It's time to add some walls, and some ground textures !


Day 11
------

The walls are up.


Day 12
------

And there was geometry.
And there was light.
And there was sprites.

`$ montage pinkhat_128.gif -tile x1 -geometry '1x1+0+0<' -alpha On -background "rgba(0,0,0,0.0)" -quality 100 pinkhat_128.png`


Day 13
------

Figured out that defining `_process`, `_input` and others actually enables them.

With the help of the GIMP, we improved our spritesheet generator.


Day 14
------

After two plugins publications and some intense bash
we're finally back in the game project itself !

Miu made a godot pixel art robot ! <3


Day 15
------

The toolbelt is under way. Miu keeps improving the character.
Some bugs were hunted. Some were left alive. Some fled far away.

Peer programming is awesome ! We, like pirates, were sailing high seas !


Day 16
------

Found a strange, glitchy behavior with system mouse cursors on controls.
Not sure if it's our fault or Godot's.

```
$ shopt -s globstar && cat **/*.gd | wc -l
```

5157 lines of gdscript so far !


Day 17
------

```
>>> print("".join([unichr(i) for i in range(5000)]))
```

I slept through the storm. The boat is still afloat, barely.
The winds are calmer, now.


Day 18
------

Thought about an editor tool to get the code to generated a specific node and its children.
Would need something to get an inst2dict diff between current node config and default node config.


D-6
---

`*.stylebox` holds binary data. Maybe it's faster than `*.tres` ? Then again, maybe not.
We're not going to use the `stylebox` extension, and styleboxes will be identified by the `stylebox` keyword in their file name.

Chests now have an interactive inventory UI ! Still a bit of a rough diamond, but showing potential !


D-5
---

Hello ? Vikings raided the coast. My world is upside-down. Will callback later.
Chests' inventories are now updated on any change wrought upon them by any source.


D-3
---

Adrenesis made a beautiful belt sprite, in between two yawns !
Getting less and less certain we'll be able to release in three days…
Miu animated the character sprite ! :D

Mostly spent the day fixing bugs in interactive inventories and floating labels.
Crafting is halfway done ; it need an UI, now.


D-0
---

Release ! Just like that tarot card Rebecca once drew !

Ended 28th of February. (and bananas)