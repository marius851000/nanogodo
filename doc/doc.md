Vector2 as Dictionary keys is possible as Vector2 is a primitive,
and it's even faster than accessing a dictionary with a string.

Classes takes longer to instantiate than writing a new dictionary,
but writing/reading an instantiated class is faster than a dictionary.

```
func _process(delta):
	# /!\ If this is overriden then is_processing() == true /!\ #
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass
```