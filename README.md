An experiment with Godot 3.

# Where is this going ?

Wherever you go, amigodo.

## Seriously ?

A hacky factorio-like, but libre, and with more hexagons.
Because who doesn't love hexagons ?

Mostly, this is an experiment, and a playground. Feel free to join!

# How to contribute ?

We need guides. Check [the wiki](https://framagit.org/godotrio/nanogodo/wikis/How-to-contribute).

# Semantics

## Character

That's the charming, friendly robot you've assumed control of.

## Pickups

The things you store in your inventory. Each pickup is actually a pickup stack.

## Fixtures

Belts, factories, etc. On the ground, hooked to the machinery ticks.

## Intakes and Outakes

**Outake**. Yes, it's spelled _outtake_.
We wrote it _outake_ everywhere, on purpose, for character-length isometry with intake.
Consider it a poetic licence? If it hurts your brain, we're sorry.

## Levels

A level is bound to a map, and has a victory condition.
It can have multiple quests (though there's no support for them yet).




# Documentation

## How to contribute

Classic git flow. Fork, change, send a merge request.

Don't hesitate to clear other people's code, and refactor as needed.




404

## Game Design Guidelines

- For atomic pickups, try to follow [CPK coloring](https://en.wikipedia.org/wiki/CPK_coloring).
- Kid safe stuff, but if you must make it nasty, do it à la [_message d'amour des dauphins_](https://www.qwant.com/?q=amour%20des%20dauphins%20illusion&r=images&t=all&o=0:23b5a1b30a838cdad417a8a8e7902fdd).
- _Have fun !_